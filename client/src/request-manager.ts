import { Group } from './interfaces/group';
import { StudyLoad } from './interfaces/study-load';
import { Subject } from './interfaces/subject';
import { SubjectPayment } from './interfaces/subject-payment';
import { SubjectType } from './interfaces/subject-type';
import { Teacher } from './interfaces/teacher';

type RequestMethod = 'get' | 'post' | 'put' | 'delete';

export class RequestManager {
    static API_URL = process.env.REACT_APP_BASE_URL + '/';

    private static async sendRequest(
        method: RequestMethod,
        requestString: string,
        body?: any,
    ) {
        try {
            const res = await fetch(requestString, {
                method: method,
                headers: {
                    'Content-Type': body
                        ? 'application/json'
                        : 'application/x-www-form-urlencoded',
                },
                body: body ? JSON.stringify(body) : null,
            });
            if (res.ok) return res;
        } catch (e) {
            console.log(e);
        }
    }

    static async fetch(method: RequestMethod, route: string, body?: any) {
        const requestString = RequestManager.API_URL + route;

        if (process.env.NODE_ENV !== 'production') {
            console.log('request url: ', requestString);
            console.log('request body: ', body ? JSON.stringify(body) : '-');
        }

        switch (method) {
            case 'get':
                return await RequestManager.sendRequest(method, requestString);
            case 'post':
                return await RequestManager.sendRequest(
                    method,
                    requestString,
                    body,
                );
            case 'put':
                return await RequestManager.sendRequest(
                    method,
                    requestString,
                    body,
                );
            case 'delete':
                return await RequestManager.sendRequest(method, requestString);
        }
    }

    static async getAllTeachers() {
        const res = await RequestManager.fetch('get', 'teachers');
        if (res) {
            const data = (await res.json()) as Teacher[];
            return data;
        }
    }

    static async getAllGroups() {
        const res = await RequestManager.fetch('get', 'groups');
        if (res) {
            const data = (await res.json()) as Group[];
            return data;
        }
    }

    static async getAllSubjects() {
        const res = await RequestManager.fetch('get', 'subjects');
        if (res) {
            const data = (await res.json()) as Subject[];
            return data;
        }
    }
    static async getAllSubjectPayments() {
        const res = await RequestManager.fetch('get', 'subject-payments');
        if (res) {
            const data = (await res.json()) as SubjectPayment[];
            return data;
        }
    }
    static async getAllSubjectTypes() {
        const res = await RequestManager.fetch('get', 'subject-types');
        if (res) {
            const data = (await res.json()) as SubjectType[];
            return data;
        }
    }
    static async getAllStudyLoads() {
        const res = await RequestManager.fetch('get', 'study-loads');
        if (res) {
            const data = (await res.json()) as StudyLoad[];
            return data;
        }
    }

    static async getTeacherWithFullData(id: number) {
        const res = await RequestManager.fetch('get', `teachers/full/${id}`);
        if (res) {
            const data = (await res.json()) as Teacher[];
            return data;
        }
    }

    static async deleteTeacher(code: number) {
        const res = await RequestManager.fetch('delete', `teachers/${code}`);
        console.log(res);
    }

    static async deleteGroup(groupNum: number) {
        const res = await RequestManager.fetch('delete', `groups/${groupNum}`);
        console.log(res);
    }

    static async deleteStudyLoad(sl: StudyLoad) {
        const res = await RequestManager.fetch(
            'delete',
            'study-loads/' +
                `teacherCode=${sl.teacherCode}&subjectNumber=${sl.subjectNumber}&subjectTypeId=${sl.subjectTypeId}&groupNumber=${sl.groupNumber}`,
        );
        console.log(res);
    }
}
