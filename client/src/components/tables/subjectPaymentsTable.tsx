import React from 'react';
import {
    Button,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { SubjectPayment } from '../../interfaces/subject-payment';
import EditSubjectPayment from '../popups/edit-subject-payment';
import { Subject } from '../../interfaces/subject';
import { SubjectType } from '../../interfaces/subject-type';

const columns = ['Subject', 'Subject type', 'Price'];

type Props = {
    reloadPage: () => void;
    subjectPayments: SubjectPayment[];
    subjects: Subject[];
    subjectTypes: SubjectType[];
};

const SubjectPaymentsTable = (props: Props) => {
    const { reloadPage, subjectPayments, subjects, subjectTypes } = props;
    const deleteRow = async (payment: SubjectPayment) => {
        await RequestManager.fetch(
            'delete',
            'subject-payments/' +
                `subjectTypeId=${payment.subjectTypeId}&subjectNumber=${payment.subjectNumber}`,
        );
        reloadPage();
    };

    return (
        <Paper>
            <Table>
                <TableHead>
                    <TableRow>
                        {columns.map((column, i) => (
                            <TableCell key={i}>{column}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {subjectPayments?.map((payment, i) => {
                        const editProps = {
                            reloadPage: reloadPage,
                            subjectPayment: payment,
                            subjects: subjects,
                            subjectTypes: subjectTypes,
                        };
                        return (
                            <TableRow key={i}>
                                <TableCell>
                                    {payment.subject?.subjectName}
                                </TableCell>

                                <TableCell>
                                    {payment.subjectType?.type}
                                </TableCell>
                                <TableCell>{payment.price}</TableCell>
                                <TableCell>
                                    <EditSubjectPayment {...editProps} />
                                    <Button
                                        onClick={() => {
                                            deleteRow(payment);
                                        }}
                                        color="secondary"
                                    >
                                        Delete
                                    </Button>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
};

export default SubjectPaymentsTable;
