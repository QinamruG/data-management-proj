import React from 'react';
import {
    Button,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { Group } from '../../interfaces/group';
import { RequestManager } from '../../request-manager';
import EditGroup from '../popups/edit-group';

const columns = ['Group number', 'Department', 'Speciality', 'Students count'];

type Props = {
    groups: Group[];
    reloadPage: () => void;
};

const GroupsTable = (props: Props) => {
    const { groups, reloadPage } = props;

    const deleteGroup = async (group: Group) => {
        await RequestManager.deleteGroup(group.groupNumber);
        reloadPage();
    };

    return (
        <Paper>
            <Table>
                <TableHead>
                    <TableRow>
                        {columns.map((column, i) => (
                            <TableCell key={i}>{column}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {groups?.map((group, i) => {
                        const editGroupProps = {
                            group: group,
                            reloadPage: reloadPage,
                        };
                        return (
                            <TableRow key={i}>
                                <TableCell>{group.groupNumber}</TableCell>
                                <TableCell>{group.groupDepartment}</TableCell>
                                <TableCell>
                                    {group.groupSpeciality ?? '-'}
                                </TableCell>
                                <TableCell>
                                    {group.groupStudentCount ?? '-'}
                                </TableCell>
                                <TableCell>
                                    <EditGroup {...editGroupProps} />
                                    <Button
                                        onClick={() => {
                                            deleteGroup(group);
                                        }}
                                        color="secondary"
                                    >
                                        Delete
                                    </Button>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
};

export default GroupsTable;
