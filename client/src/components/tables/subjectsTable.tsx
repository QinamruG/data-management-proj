import {
    Button,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { Subject } from '../../interfaces/subject';
import EditSubject from '../popups/edit-subject';

const columns = ['Subject number', 'Subject name'];

type Props = {
    subjects: Subject[];
    reloadPage: () => void;
};

const SubjectsTable = (props: Props) => {
    const { subjects, reloadPage } = props;

    const deleteRow = async (subjNum: number) => {
        await RequestManager.fetch('delete', `subjects/${subjNum}`);
        reloadPage();
    };

    return (
        <Paper>
            <Table>
                <TableHead>
                    <TableRow>
                        {columns.map((column, i) => (
                            <TableCell key={i}>{column}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {subjects?.map((item, i) => {
                        const editProps = {
                            reloadPage: reloadPage,
                            subject: item,
                        };
                        return (
                            <TableRow key={i}>
                                <TableCell>{item.subjectNumber}</TableCell>
                                <TableCell>{item.subjectName}</TableCell>
                                <TableCell>
                                    <EditSubject {...editProps} />
                                    <Button
                                        onClick={() => {
                                            deleteRow(item.subjectNumber);
                                        }}
                                        color="secondary"
                                    >
                                        Delete
                                    </Button>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
};

export default SubjectsTable;
