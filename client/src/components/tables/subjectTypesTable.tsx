import {
    Button,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { SubjectType } from '../../interfaces/subject-type';
import EditSubjectType from '../popups/edit-subject-type';

const columns = ['Type id', 'Type name'];

type Props = {
    subjectTypes: SubjectType[];
    reloadPage: () => void;
};

const SubjectTypesTable = (props: Props) => {
    const { subjectTypes, reloadPage } = props;

    const deleteRow = async (typeId: number) => {
        await RequestManager.fetch('delete', `subject-types/${typeId}`);
        reloadPage();
    };

    return (
        <Paper>
            <Table>
                <TableHead>
                    <TableRow>
                        {columns.map((column, i) => (
                            <TableCell key={i}>{column}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {subjectTypes?.map((item, i) => {
                        const editProps = {
                            reloadPage: reloadPage,
                            subjectType: item,
                        };
                        return (
                            <TableRow key={i}>
                                <TableCell>{item.subjectTypeId}</TableCell>
                                <TableCell>{item.type}</TableCell>
                                <TableCell>
                                    <EditSubjectType {...editProps} />
                                    <Button
                                        onClick={() => {
                                            deleteRow(item.subjectTypeId);
                                        }}
                                        color="secondary"
                                    >
                                        Delete
                                    </Button>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
};

export default SubjectTypesTable;
