import { Teacher } from '../../interfaces/teacher';
import { RequestManager } from '../../request-manager';
import {
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Button,
    Paper,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
} from '@material-ui/core';
import EditTeacher from '../popups/edit-teacher';
import { Subject } from '../../interfaces/subject';
import { useState } from 'react';
import styles from './teachersTable.module.css';

const columns = [
    'Code',
    'Name',
    'Second name',
    'Surname',
    'Phone',
    'Exp',
    'Subjects',
    'Actions',
];

type Props = {
    teachers: Teacher[];
    subjects: Subject[];
    reloadPage: () => void;
};

const TeachersTable = (props: Props) => {
    const { teachers, reloadPage, subjects } = props;
    const [subjectNumber, setSubjectNumber] = useState<number>(0);
    const deleteRow = async (index: number) => {
        await RequestManager.deleteTeacher(teachers[index].teacherCode);
        reloadPage();
    };

    const deleteSubjectFromSubjectList = async (
        subjectNumber: number,
        teacherCode: number,
    ) => {
        await RequestManager.fetch(
            'delete',
            'teachers-subjects/' +
                `subjectNumber=${subjectNumber}&teacherCode=${teacherCode}`,
        );
        reloadPage();
    };

    const addSubjectForTeacher = async (
        subjectNumber: number,
        teacherCode: number,
    ) => {
        await RequestManager.fetch('post', 'teachers-subjects', {
            subjectNumber: subjectNumber,
            teacherCode: teacherCode,
        });
        setSubjectNumber(0);
        reloadPage();
    };

    return (
        <Paper>
            <Table>
                <TableHead>
                    <TableRow>
                        {columns.map((column, i) => (
                            <TableCell key={i}>{column}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {teachers?.map((teacher, i) => {
                        const editProps = {
                            reloadPage: reloadPage,
                            teacher: teacher,
                        };
                        return (
                            <TableRow key={i}>
                                <TableCell>{teacher.teacherCode}</TableCell>
                                <TableCell>{teacher.teacherName}</TableCell>
                                <TableCell>
                                    {teacher?.teacherSecondName ?? '-'}
                                </TableCell>
                                <TableCell>
                                    {teacher.teacherSurname ?? '-'}
                                </TableCell>
                                <TableCell>
                                    {teacher.teacherPhone ?? '-'}
                                </TableCell>
                                <TableCell>
                                    {teacher.teacherExperience ?? '-'}
                                </TableCell>
                                <TableCell>
                                    <div className={styles['subjects-cell']}>
                                        <div
                                            className={styles['subjects-list']}
                                        >
                                            <ul>
                                                {teacher?.subjects?.map(
                                                    (subject, i) => (
                                                        <li key={i}>
                                                            <div>
                                                                {
                                                                    subject.subjectName
                                                                }
                                                                <Button
                                                                    onClick={() => {
                                                                        deleteSubjectFromSubjectList(
                                                                            subject.subjectNumber,
                                                                            teacher.teacherCode,
                                                                        );
                                                                    }}
                                                                    color="secondary"
                                                                >
                                                                    Delete
                                                                </Button>
                                                            </div>
                                                        </li>
                                                    ),
                                                )}
                                            </ul>
                                        </div>
                                        <div
                                            className={
                                                styles['add-subject-form']
                                            }
                                        >
                                            <FormControl>
                                                <InputLabel shrink>
                                                    Add subject
                                                </InputLabel>
                                                <Select
                                                    displayEmpty
                                                    value={subjectNumber.toString()}
                                                    name="add-subject"
                                                    onChange={(e) =>
                                                        setSubjectNumber(
                                                            Number(
                                                                e.target.value,
                                                            ),
                                                        )
                                                    }
                                                >
                                                    <MenuItem value={0} key={0}>
                                                        {'Not assigned'}
                                                    </MenuItem>
                                                    {subjects?.map((item) => {
                                                        return (
                                                            <MenuItem
                                                                value={
                                                                    item.subjectNumber
                                                                }
                                                                key={
                                                                    item.subjectNumber
                                                                }
                                                            >
                                                                {`${item.subjectName}`}
                                                            </MenuItem>
                                                        );
                                                    })}
                                                </Select>
                                                <Button
                                                    onClick={() => {
                                                        addSubjectForTeacher(
                                                            subjectNumber,
                                                            teacher.teacherCode,
                                                        );
                                                    }}
                                                    color="primary"
                                                >
                                                    Add
                                                </Button>
                                            </FormControl>
                                        </div>
                                    </div>
                                </TableCell>
                                <TableCell>
                                    <EditTeacher {...editProps} />
                                    <Button
                                        onClick={() => {
                                            deleteRow(i as number);
                                        }}
                                        color="secondary"
                                    >
                                        Delete
                                    </Button>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
};

export default TeachersTable;
