import React from 'react';
import {
    Button,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { StudyLoad } from '../../interfaces/study-load';
import { Teacher } from '../../interfaces/teacher';
import EditStudyLoad from '../popups/edit-study-load';
import { Subject } from '../../interfaces/subject';
import { SubjectType } from '../../interfaces/subject-type';
import { Group } from '../../interfaces/group';

const columns = [
    'Group number',
    'Teacher',
    'Subject',
    'Subject type',
    'Hours',
    'Price',
];

type Props = {
    studyLoads: StudyLoad[];
    teachers: Teacher[];
    subjects: Subject[];
    subjectTypes: SubjectType[];
    groups: Group[];
    reloadPage: () => void;
};

const showTeacherName = (teacher: Teacher | undefined) => {
    if (!teacher) return '';
    let str = teacher.teacherSurname ?? '';
    str += str.length ? ' ' : '';
    str += teacher.teacherName.substring(0, 1) + '.';
    str += teacher.teacherSecondName
        ? teacher.teacherSecondName.substring(0, 1) + ''
        : '';
    return str;
};

const calculatePrice = (hours: number | null, price: number | undefined) => {
    if (hours && price) return (hours * price).toString();
    return '-';
};

const StudyLoadsTable = (props: Props) => {
    const { studyLoads, reloadPage, teachers, subjects, subjectTypes, groups } =
        props;
    const deleteStudyLoad = async (studyLoad: StudyLoad) => {
        await RequestManager.deleteStudyLoad(studyLoad);
        reloadPage();
    };

    return (
        <Paper>
            <Table>
                <TableHead>
                    <TableRow>
                        {columns.map((column, i) => (
                            <TableCell key={i}>{column}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {studyLoads?.map((studyLoad, i) => {
                        const editStudyLoadProps = {
                            reloadPage: reloadPage,
                            studyLoad: studyLoad,
                            teachers: teachers,
                            subjects: subjects,
                            subjectTypes: subjectTypes,
                            groups: groups,
                        };
                        return (
                            <TableRow key={i}>
                                <TableCell>{studyLoad.groupNumber}</TableCell>
                                <TableCell>
                                    {showTeacherName(studyLoad.teacher)}
                                </TableCell>
                                <TableCell>
                                    {studyLoad.subject?.subjectName}
                                </TableCell>
                                <TableCell>
                                    {studyLoad.subjectType?.type}
                                </TableCell>
                                <TableCell>{studyLoad.hours}</TableCell>
                                <TableCell>
                                    {calculatePrice(
                                        studyLoad.hours,
                                        studyLoad.subjectPayment?.price,
                                    )}
                                </TableCell>
                                <TableCell>
                                    <EditStudyLoad {...editStudyLoadProps} />
                                    <Button
                                        onClick={() => {
                                            deleteStudyLoad(studyLoad);
                                        }}
                                        color="secondary"
                                    >
                                        Delete
                                    </Button>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
};

export default StudyLoadsTable;
