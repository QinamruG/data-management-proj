import React, { useState } from 'react';
import {
    TextField,
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { Subject } from '../../interfaces/subject';
import { SubjectType } from '../../interfaces/subject-type';

type Props = {
    reloadPage: () => void;
    subjects: Subject[];
    subjectTypes: SubjectType[];
};

const AddSubjectPayment = (props: Props) => {
    const { reloadPage, subjects, subjectTypes } = props;

    const [price, setPrice] = useState<number>(0);
    const [subjectNumber, setSubjectNumber] = useState<number>(0);
    const [subjectTypeId, setSubjectTypeId] = useState<number>(0);

    const resetForms = () => {
        setPrice(0);
        setSubjectNumber(0);
        setSubjectTypeId(0);
    };

    const addRow = async () => {
        await RequestManager.fetch('post', 'subject-payments', {
            subjectNumber: subjectNumber,
            subjectTypeId: subjectTypeId,
            price: price,
        });
        resetForms();
        reloadPage();
    };

    return (
        <div>
            <FormControl>
                <InputLabel shrink>Subject</InputLabel>
                <Select
                    displayEmpty
                    value={subjectNumber.toString()}
                    name="subject"
                    onChange={(e) => setSubjectNumber(Number(e.target.value))}
                >
                    <MenuItem value={0} key={0}>
                        {'Not assigned'}
                    </MenuItem>
                    {subjects?.map((item) => {
                        return (
                            <MenuItem
                                value={item.subjectNumber}
                                key={item.subjectNumber}
                            >
                                {`${item.subjectName}`}
                            </MenuItem>
                        );
                    })}
                </Select>
            </FormControl>
            <FormControl>
                <InputLabel shrink>Subject type</InputLabel>
                <Select
                    displayEmpty
                    value={subjectTypeId.toString()}
                    name="subject type"
                    onChange={(e) => setSubjectTypeId(Number(e.target.value))}
                >
                    <MenuItem value={0} key={0}>
                        {'Not assigned'}
                    </MenuItem>
                    {subjectTypes?.map((item) => {
                        return (
                            <MenuItem
                                value={item.subjectTypeId}
                                key={item.subjectTypeId}
                            >
                                {`${item.type}`}
                            </MenuItem>
                        );
                    })}
                </Select>
            </FormControl>
            <TextField
                id="price-count"
                label="Price"
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                value={price.toString()}
                onChange={(e) => {
                    setPrice(Number(e.target.value));
                }}
            />
            <Button onClick={() => addRow()} color="primary" variant="outlined">
                Add
            </Button>
        </div>
    );
};

export default AddSubjectPayment;
