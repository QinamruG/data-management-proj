import React, { useState } from 'react';
import { TextField, Button } from '@material-ui/core';
import { RequestManager } from '../../request-manager';

type Props = {
    reloadPage: () => void;
};

const FindGroup = (props: Props) => {
    const { reloadPage } = props;
    const [groupNumber, setGroupNumber] = useState<number>(0);
    const [department, setDepartment] = useState<string>('');
    const [speciality, setSpeciality] = useState<string>('');
    const [studentsCount, setStudentsCount] = useState<number>(0);

    const resetForms = () => {
        setGroupNumber(0);
        setDepartment('');
        setSpeciality('');
        setStudentsCount(0);
    };

    const addRow = async () => {
        await RequestManager.fetch('post', 'groups', {
            groupNumber: groupNumber,

            groupSpeciality: speciality,

            groupDepartment: department,

            groupStudentCount: studentsCount,
        });
        resetForms();
        reloadPage();
    };

    return (
        <div>
            <TextField
                id="add-group-number"
                label="Group Number"
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                // helperText="enter group number"
                value={groupNumber.toString()}
                onChange={(e) => {
                    setGroupNumber(Number(e.target.value));
                }}
            />
            <TextField
                id="name"
                label="Department"
                type="text"
                InputLabelProps={{
                    shrink: true,
                }}
                value={department}
                onChange={(e) => {
                    setDepartment(e.target.value);
                }}
            />
            <TextField
                id="name"
                label="Speciality"
                type="text"
                InputLabelProps={{
                    shrink: true,
                }}
                value={speciality}
                onChange={(e) => {
                    setSpeciality(e.target.value);
                }}
            />
            <TextField
                id="add-group-number"
                label="Count of students"
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                // helperText="enter group number"
                value={studentsCount.toString()}
                onChange={(e) => {
                    setStudentsCount(Number(e.target.value));
                }}
            />

            <Button onClick={() => addRow()} color="primary" variant="outlined">
                Add
            </Button>
        </div>
    );
};

export default FindGroup;
