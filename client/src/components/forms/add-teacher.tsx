import React, { useState } from 'react';
import { TextField, Button } from '@material-ui/core';
import { RequestManager } from '../../request-manager';

type Props = {
    reloadPage: () => void;
};

const AddTeacher = (props: Props) => {
    const { reloadPage } = props;
    const [teacherCode, setTeacherCode] = useState<number>(0);
    const [teacherPhone, setTeacherPhone] = useState<number>(0);
    const [teacherName, setTeacherName] = useState<string>('');
    const [teacherSecondName, setTeacherSecondName] = useState<string>('');
    const [teacherSurname, setTeacherSurname] = useState<string>('');
    const [teacherExperience, setTeacherExperience] = useState<number>(0);

    const resetForms = () => {
        setTeacherCode(0);
        setTeacherName('');
        setTeacherSecondName('');
        setTeacherSurname('');
        setTeacherPhone(0);
        setTeacherExperience(0);
    };

    const addRow = async () => {
        await RequestManager.fetch('post', 'teachers', {
            teacherCode: teacherCode > 0 ? teacherCode : null,
            teacherPhone: teacherPhone,
            teacherName: teacherName,
            teacherSecondName: teacherSecondName,
            teacherSurname: teacherSurname,
            teacherExperience: teacherExperience,
        });
        resetForms();
        reloadPage();
    };

    return (
        <div>
            <TextField
                id="add-teacher-code"
                label="Teacher code"
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                value={teacherCode > 0 ? teacherCode.toString() : ''}
                onChange={(e) => {
                    setTeacherCode(Number(e.target.value));
                }}
            />
            <TextField
                id="add-teacher-phone"
                label="Phone"
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                value={teacherPhone.toString()}
                onChange={(e) => {
                    setTeacherPhone(Number(e.target.value));
                }}
            />
            <TextField
                id="add-teacher-name"
                label="Name"
                type="text"
                InputLabelProps={{
                    shrink: true,
                }}
                value={teacherName}
                onChange={(e) => {
                    setTeacherName(e.target.value);
                }}
            />
            <TextField
                id="add-teacher-second-name"
                label="Second name"
                type="text"
                InputLabelProps={{
                    shrink: true,
                }}
                value={teacherSecondName}
                onChange={(e) => {
                    setTeacherSecondName(e.target.value);
                }}
            />
            <TextField
                id="add-teacher-surname"
                label="Surname"
                type="text"
                InputLabelProps={{
                    shrink: true,
                }}
                value={teacherSurname}
                onChange={(e) => {
                    setTeacherSurname(e.target.value);
                }}
            />
            <TextField
                id="add-teacher-experience"
                label="Experience"
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                value={teacherExperience.toString()}
                onChange={(e) => {
                    setTeacherExperience(Number(e.target.value));
                }}
            />

            <Button onClick={() => addRow()} color="primary" variant="outlined">
                Add
            </Button>
        </div>
    );
};

export default AddTeacher;
