import React, { useState } from 'react';
import { TextField, Button } from '@material-ui/core';
import { RequestManager } from '../../request-manager';

type Props = {
    reloadPage: () => void;
};

const AddSubject = (props: Props) => {
    const { reloadPage } = props;
    const [subjectName, setSubjectName] = useState<string>('');
    const [subjectNumber, setSubjectNumber] = useState<number>(0);

    const resetForms = () => {
        setSubjectName('');
        setSubjectNumber(0);
    };

    const addRow = async () => {
        console.log('add row');
        await RequestManager.fetch('post', 'subjects', {
            subjectNumber: subjectNumber,
            subjectName: subjectName,
        });
        resetForms();
        reloadPage();
    };

    return (
        <div>
            <TextField
                id="subject-number"
                label="Subject number"
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                value={subjectNumber.toString()}
                onChange={(e) => {
                    setSubjectNumber(Number(e.target.value));
                }}
            />
            <TextField
                id="subject-name"
                label="Subject name"
                type="text"
                InputLabelProps={{
                    shrink: true,
                }}
                value={subjectName}
                onChange={(e) => {
                    setSubjectName(e.target.value);
                }}
            />
            <Button onClick={() => addRow()} color="primary" variant="outlined">
                Add
            </Button>
        </div>
    );
};

export default AddSubject;
