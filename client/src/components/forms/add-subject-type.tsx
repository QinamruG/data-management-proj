import React, { useState } from 'react';
import { TextField, Button } from '@material-ui/core';
import { RequestManager } from '../../request-manager';

type Props = {
    reloadPage: () => void;
};

const AddSubjectType = (props: Props) => {
    const { reloadPage } = props;
    const [subjectTypeName, setSubjectTypeName] = useState<string>('');
    const [subjectTypeId, setSubjectTypeId] = useState<number>(0);

    const resetForms = () => {
        setSubjectTypeName('');
        setSubjectTypeId(0);
    };

    const addRow = async () => {
        console.log('add row');
        await RequestManager.fetch('post', 'subject-types', {
            subjectTypeId: subjectTypeId,
            type: subjectTypeName,
        });
        resetForms();
        reloadPage();
    };

    return (
        <div>
            <TextField
                id="type-id"
                label="Type id"
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                value={subjectTypeId.toString()}
                onChange={(e) => {
                    setSubjectTypeId(Number(e.target.value));
                }}
            />
            <TextField
                id="type-name"
                label="Type name"
                type="text"
                InputLabelProps={{
                    shrink: true,
                }}
                value={subjectTypeName}
                onChange={(e) => {
                    setSubjectTypeName(e.target.value);
                }}
            />
            <Button onClick={() => addRow()} color="primary" variant="outlined">
                Add
            </Button>
        </div>
    );
};

export default AddSubjectType;
