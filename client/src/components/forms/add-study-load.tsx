import React, { useState } from 'react';
import {
    TextField,
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { Teacher } from '../../interfaces/teacher';
import { Subject } from '../../interfaces/subject';
import { SubjectType } from '../../interfaces/subject-type';
import { Group } from '../../interfaces/group';

type Props = {
    reloadPage: () => void;
    teachers: Teacher[];
    subjects: Subject[];
    subjectTypes: SubjectType[];
    groups: Group[];
};

const AddStudyLoad = (props: Props) => {
    const { reloadPage, teachers, subjects, subjectTypes, groups } = props;
    const [teacherCode, setTeacherCode] = useState<number>(0);
    const [hours, setHours] = useState<number>(0);
    const [subjectNumber, setSubjectNumber] = useState<number>(0);
    const [subjectTypeId, setSubjectTypeId] = useState<number>(0);
    const [groupNumber, setGroupNumber] = useState<number>(0);

    const resetForms = () => {
        setTeacherCode(0);
        setHours(0);
        setSubjectNumber(0);
        setSubjectTypeId(0);
        setGroupNumber(0);
    };
    const addRow = async () => {
        console.log('add row', {
            teacherCode: teacherCode,
            hours: hours,
            subjectNumber: subjectNumber,
            subjectTypeId: subjectTypeId,
            groupNumber: groupNumber,
        });
        await RequestManager.fetch('post', 'study-loads', {
            teacherCode: teacherCode,
            hours: hours,
            subjectNumber: subjectNumber,
            subjectTypeId: subjectTypeId,
            groupNumber: groupNumber,
        });
        resetForms();
        reloadPage();
    };
    return (
        <div>
            <FormControl>
                <InputLabel shrink>Teacher</InputLabel>
                <Select
                    displayEmpty
                    value={teacherCode.toString()}
                    name="teacher"
                    onChange={(e) => setTeacherCode(Number(e.target.value))}
                >
                    <MenuItem value={0} key={0}>
                        {'Not assigned'}
                    </MenuItem>
                    {teachers?.map((item) => {
                        return (
                            <MenuItem
                                value={item.teacherCode}
                                key={item.teacherCode}
                            >
                                {`${item.teacherName} ${item.teacherSurname}`}
                            </MenuItem>
                        );
                    })}
                </Select>
            </FormControl>
            <FormControl>
                <InputLabel shrink>Subject</InputLabel>
                <Select
                    displayEmpty
                    value={subjectNumber.toString()}
                    name="subject"
                    onChange={(e) => setSubjectNumber(Number(e.target.value))}
                >
                    <MenuItem value={0} key={0}>
                        {'Not assigned'}
                    </MenuItem>
                    {subjects?.map((item) => {
                        return (
                            <MenuItem
                                value={item.subjectNumber}
                                key={item.subjectNumber}
                            >
                                {`${item.subjectName}`}
                            </MenuItem>
                        );
                    })}
                </Select>
            </FormControl>
            <FormControl>
                <InputLabel shrink>Subject type</InputLabel>
                <Select
                    displayEmpty
                    value={subjectTypeId.toString()}
                    name="subject type"
                    onChange={(e) => setSubjectTypeId(Number(e.target.value))}
                >
                    <MenuItem value={0} key={0}>
                        {'Not assigned'}
                    </MenuItem>
                    {subjectTypes?.map((item) => {
                        return (
                            <MenuItem
                                value={item.subjectTypeId}
                                key={item.subjectTypeId}
                            >
                                {`${item.type}`}
                            </MenuItem>
                        );
                    })}
                </Select>
            </FormControl>
            <FormControl>
                <InputLabel shrink>Group</InputLabel>
                <Select
                    displayEmpty
                    value={groupNumber.toString()}
                    name="subject type"
                    onChange={(e) => setGroupNumber(Number(e.target.value))}
                >
                    <MenuItem value={0} key={0}>
                        {'Not assigned'}
                    </MenuItem>
                    {groups?.map((item) => {
                        return (
                            <MenuItem
                                value={item.groupNumber}
                                key={item.groupNumber}
                            >
                                {`${item.groupNumber} ${item.groupDepartment}`}
                            </MenuItem>
                        );
                    })}
                </Select>
            </FormControl>
            <TextField
                id="add-sl-hours"
                label="Hours"
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                value={hours.toString()}
                onChange={(e) => {
                    setHours(Number(e.target.value));
                }}
            />
            <Button onClick={() => addRow()} color="primary" variant="outlined">
                Add
            </Button>
        </div>
    );
};

export default AddStudyLoad;
