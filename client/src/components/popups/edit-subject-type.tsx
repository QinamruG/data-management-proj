import React, { useState } from 'react';
import {
    Dialog,
    DialogActions,
    DialogTitle,
    DialogContent,
    TextField,
    Button,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { SubjectType } from '../../interfaces/subject-type';

type Props = {
    reloadPage: () => void;
    subjectType: SubjectType;
};

const EditSubjectType = (props: Props) => {
    const { subjectType, reloadPage } = props;
    const [subjectTypeId, setSubjectTypeId] = useState<number | null>(
        () => subjectType.subjectTypeId,
    );
    const [type, setType] = useState<string>(
        () => subjectType.type,
    );
    const [open, setOpen] = React.useState(false);

    const setStartValues = () => {
        setSubjectTypeId(subjectType.subjectTypeId);
        setType(subjectType.type);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
        setStartValues();
    };

    const handleSaveChanges = async () => {
        await RequestManager.fetch(
            'put',
            `subject-types/${subjectType.subjectTypeId}`,
            {
                subjectTypeId: subjectTypeId,
                type: type,
            },
        );
        reloadPage();
        setOpen(false);
    };

    return (
        <>
            <Button color="primary" onClick={handleClickOpen}>
                Edit
            </Button>
            <Dialog
                maxWidth="lg"
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Edit subject</DialogTitle>
                <DialogContent>
                    <TextField
                        id="number"
                        label="Number"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={subjectTypeId ? subjectTypeId.toString() : ''}
                        onChange={(e) => {
                            setSubjectTypeId(Number(e.target.value));
                        }}
                    />
                    <TextField
                        id="name"
                        label="Name"
                        type="text"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={type}
                        onChange={(e) => {
                            setType(e.target.value);
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={handleSaveChanges} color="primary">
                        Save changes
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default EditSubjectType;
