import React, { useState } from 'react';
import {
    Dialog,
    DialogActions,
    DialogTitle,
    DialogContent,
    TextField,
    Button,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { Teacher } from '../../interfaces/teacher';

type Props = {
    reloadPage: () => void;
    teacher: Teacher;
};

const EditTeacher = (props: Props) => {
    const { teacher, reloadPage } = props;
    const [teacherPhone, setTeacherPhone] = useState<number | null>(
        () => teacher.teacherPhone,
    );
    const [teacherName, setTeacherName] = useState<string>(
        () => teacher.teacherName,
    );
    const [teacherSecondName, setTeacherSecondName] = useState<string | null>(
        () => teacher.teacherSecondName,
    );
    const [teacherSurname, setTeacherSurname] = useState<string | null>(
        () => teacher.teacherSurname,
    );
    const [teacherExperience, setTeacherExperience] = useState<number | null>(
        () => teacher.teacherExperience,
    );

    const setStartValues = () => {
        setTeacherPhone(teacher.teacherPhone);
        setTeacherName(teacher.teacherName);
        setTeacherSecondName(teacher.teacherSecondName);
        setTeacherSurname(teacher.teacherSurname);
        setTeacherExperience(teacher.teacherExperience);
    };

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
        setStartValues();
    };

    const handleSaveChanges = async () => {
        await RequestManager.fetch('put', `teachers/${teacher.teacherCode}`, {
            teacherPhone: teacherPhone,
            teacherName: teacherName,
            teacherSecondName: teacherSecondName,
            teacherSurname: teacherSurname,
            teacherExperience: teacherExperience,
        });
        reloadPage();
        setOpen(false);
    };

    return (
        <>
            <Button color="primary" onClick={handleClickOpen}>
                Edit
            </Button>
            <Dialog
                maxWidth="lg"
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Edit teacher</DialogTitle>
                <DialogContent>
                    <TextField
                        id="phone"
                        label="Phone"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={teacherPhone ? teacherPhone.toString() : ''}
                        onChange={(e) => {
                            setTeacherPhone(Number(e.target.value));
                        }}
                    />
                    <TextField
                        id="name"
                        label="Name"
                        type="text"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={teacherName}
                        onChange={(e) => {
                            setTeacherName(e.target.value);
                        }}
                    />
                    <TextField
                        id="sec-name"
                        label="Second name"
                        type="text"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={teacherSecondName}
                        onChange={(e) => {
                            setTeacherSecondName(e.target.value);
                        }}
                    />
                    <TextField
                        id="surname"
                        label="Surname"
                        type="text"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={teacherSurname}
                        onChange={(e) => {
                            setTeacherSurname(e.target.value);
                        }}
                    />
                    <TextField
                        id="exp"
                        label="Experience"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={
                            teacherExperience
                                ? teacherExperience.toString()
                                : '0'
                        }
                        onChange={(e) => {
                            setTeacherExperience(Number(e.target.value));
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={handleSaveChanges} color="primary">
                        Save changes
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default EditTeacher;
