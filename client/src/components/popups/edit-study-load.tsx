import React, { useState } from 'react';
import {
    Dialog,
    DialogActions,
    DialogTitle,
    DialogContent,
    TextField,
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
} from '@material-ui/core';
import { Group } from '../../interfaces/group';
import { RequestManager } from '../../request-manager';
import { StudyLoad } from '../../interfaces/study-load';
import { Teacher } from '../../interfaces/teacher';
import { Subject } from '../../interfaces/subject';
import { SubjectType } from '../../interfaces/subject-type';

type Props = {
    reloadPage: () => void;
    studyLoad: StudyLoad;
    teachers: Teacher[];
    subjects: Subject[];
    subjectTypes: SubjectType[];
    groups: Group[];
};

const EditStudyLoad = (props: Props) => {
    const { studyLoad, reloadPage, teachers, subjects, subjectTypes, groups } =
        props;
    const [open, setOpen] = useState(false);
    const [teacherCode, setTeacherCode] = useState<number>(
        () => studyLoad.teacherCode,
    );
    const [hours, setHours] = useState<number | null>(() => studyLoad.hours);
    const [subjectNumber, setSubjectNumber] = useState<number>(
        () => studyLoad.subjectNumber,
    );
    const [subjectTypeId, setSubjectTypeId] = useState<number>(
        () => studyLoad.subjectTypeId,
    );
    const [groupNumber, setGroupNumber] = useState<number>(
        () => studyLoad.groupNumber,
    );

    const setStartValues = () => {
        setTeacherCode(studyLoad.teacherCode);
        setHours(studyLoad.hours);
        setSubjectNumber(studyLoad.subjectNumber);
        setSubjectTypeId(studyLoad.subjectTypeId);
        setGroupNumber(studyLoad.groupNumber);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
        setStartValues();
    };

    const handleSaveChanges = async () => {
        await RequestManager.fetch('put', 'study-loads', {
            teacherCode: teacherCode,
            hours: hours,
            subjectNumber: subjectNumber,
            subjectTypeId: subjectTypeId,
            groupNumber: groupNumber,
        });
        reloadPage();
        setOpen(false);
    };

    return (
        <>
            <Button color="primary" onClick={handleClickOpen}>
                Edit
            </Button>
            <Dialog
                maxWidth="lg"
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Edit study load</DialogTitle>
                <DialogContent>
                    <FormControl>
                        <InputLabel shrink>Teacher</InputLabel>
                        <Select
                            displayEmpty
                            value={teacherCode.toString()}
                            name="teacher"
                            onChange={(e) =>
                                setTeacherCode(Number(e.target.value))
                            }
                        >
                            <MenuItem value={0} key={0}>
                                {'Not assigned'}
                            </MenuItem>
                            {teachers?.map((item) => {
                                return (
                                    <MenuItem
                                        value={item.teacherCode}
                                        key={item.teacherCode}
                                    >
                                        {`${item.teacherName} ${item.teacherSurname}`}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <FormControl>
                        <InputLabel shrink>Subject</InputLabel>
                        <Select
                            displayEmpty
                            value={subjectNumber.toString()}
                            name="subject"
                            onChange={(e) =>
                                setSubjectNumber(Number(e.target.value))
                            }
                        >
                            <MenuItem value={0} key={0}>
                                {'Not assigned'}
                            </MenuItem>
                            {subjects?.map((item) => {
                                return (
                                    <MenuItem
                                        value={item.subjectNumber}
                                        key={item.subjectNumber}
                                    >
                                        {`${item.subjectName}`}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <FormControl>
                        <InputLabel shrink>Subject type</InputLabel>
                        <Select
                            displayEmpty
                            value={subjectTypeId.toString()}
                            name="subject type"
                            onChange={(e) =>
                                setSubjectTypeId(Number(e.target.value))
                            }
                        >
                            <MenuItem value={0} key={0}>
                                {'Not assigned'}
                            </MenuItem>
                            {subjectTypes?.map((item) => {
                                return (
                                    <MenuItem
                                        value={item.subjectTypeId}
                                        key={item.subjectTypeId}
                                    >
                                        {`${item.type}`}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <FormControl>
                        <InputLabel shrink>Group</InputLabel>
                        <Select
                            displayEmpty
                            value={groupNumber.toString()}
                            name="subject type"
                            onChange={(e) =>
                                setGroupNumber(Number(e.target.value))
                            }
                        >
                            <MenuItem value={0} key={0}>
                                {'Not assigned'}
                            </MenuItem>
                            {groups?.map((item) => {
                                return (
                                    <MenuItem
                                        value={item.groupNumber}
                                        key={item.groupNumber}
                                    >
                                        {`${item.groupNumber} ${item.groupDepartment}`}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <TextField
                        id="add-sl-hours"
                        label="Hours"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={hours ? hours.toString() : ''}
                        onChange={(e) => {
                            setHours(Number(e.target.value));
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={handleSaveChanges} color="primary">
                        Save changes
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default EditStudyLoad;
