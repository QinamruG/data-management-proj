import React, { useState } from 'react';
import {
    Dialog,
    DialogActions,
    DialogTitle,
    DialogContent,
    TextField,
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { SubjectPayment } from '../../interfaces/subject-payment';
import { Subject } from '../../interfaces/subject';
import { SubjectType } from '../../interfaces/subject-type';

type Props = {
    reloadPage: () => void;
    subjectPayment: SubjectPayment;
    subjects: Subject[];
    subjectTypes: SubjectType[];
};

const EditSubjectPayment = (props: Props) => {
    const { subjectPayment, reloadPage, subjects, subjectTypes } = props;
    const [price, setPrice] = useState<number>(() => subjectPayment.price);
    const [subjectNumber, setSubjectNumber] = useState<number>(
        () => subjectPayment.subjectNumber,
    );
    const [subjectTypeId, setSubjectTypeId] = useState<number>(
        () => subjectPayment.subjectTypeId,
    );
    const [open, setOpen] = React.useState(false);

    const setStartValues = () => {
        setSubjectNumber(subjectPayment.subjectNumber);
        setSubjectTypeId(subjectPayment.subjectTypeId);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
        setStartValues();
    };

    const handleSaveChanges = async () => {
        await RequestManager.fetch(
            'put',
            `subject-payments/subjectTypeId=${subjectTypeId}&subjectNumber=${subjectNumber}`,
            {
                subjectTypeId: subjectTypeId,
                subjectNumber: subjectNumber,
                price: price,
            },
        );
        reloadPage();
        setOpen(false);
    };

    return (
        <>
            <Button color="primary" onClick={handleClickOpen}>
                Edit
            </Button>
            <Dialog
                maxWidth="lg"
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Edit subject</DialogTitle>
                <DialogContent>
                    <FormControl>
                        <InputLabel shrink>Subject</InputLabel>
                        <Select
                            displayEmpty
                            value={subjectNumber.toString()}
                            name="subject"
                            onChange={(e) =>
                                setSubjectNumber(Number(e.target.value))
                            }
                        >
                            <MenuItem value={0} key={0}>
                                {'Not assigned'}
                            </MenuItem>
                            {subjects?.map((item) => {
                                return (
                                    <MenuItem
                                        value={item.subjectNumber}
                                        key={item.subjectNumber}
                                    >
                                        {`${item.subjectName}`}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <FormControl>
                        <InputLabel shrink>Subject type</InputLabel>
                        <Select
                            displayEmpty
                            value={subjectTypeId.toString()}
                            name="subject type"
                            onChange={(e) =>
                                setSubjectTypeId(Number(e.target.value))
                            }
                        >
                            <MenuItem value={0} key={0}>
                                {'Not assigned'}
                            </MenuItem>
                            {subjectTypes?.map((item) => {
                                return (
                                    <MenuItem
                                        value={item.subjectTypeId}
                                        key={item.subjectTypeId}
                                    >
                                        {`${item.type}`}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <TextField
                        id="price-count"
                        label="Price"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={price.toString()}
                        onChange={(e) => {
                            setPrice(Number(e.target.value));
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={handleSaveChanges} color="primary">
                        Save changes
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default EditSubjectPayment;
