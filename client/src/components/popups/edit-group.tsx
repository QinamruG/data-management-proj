import React, { useState } from 'react';
import {
    Dialog,
    DialogActions,
    DialogTitle,
    DialogContent,
    TextField,
    Button,
} from '@material-ui/core';
import { Group } from '../../interfaces/group';
import { RequestManager } from '../../request-manager';

type Props = {
    reloadPage: () => void;
    group: Group;
};

const EditGroup = (props: Props) => {
    const { group, reloadPage } = props;
    const [groupNumber, setGroupNumber] = useState<number>(
        () => group.groupNumber,
    );
    const [department, setDepartment] = useState<string>(
        () => group.groupDepartment,
    );
    const [speciality, setSpeciality] = useState<string>(
        () => group.groupSpeciality,
    );
    const [studentsCount, setStudentsCount] = useState<number | null>(
        () => group.groupStudentCount,
    );

    const setStartValues = () => {
        setGroupNumber(group.groupNumber);
        setDepartment(group.groupDepartment);
        setSpeciality(group.groupSpeciality);
        setStudentsCount(group.groupStudentCount);
    };

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
        setStartValues();
    };

    const handleSaveChanges = () => {
        RequestManager.fetch('put', 'groups', {
            groupNumber: groupNumber,
            groupSpeciality: speciality,
            groupDepartment: department,
            groupStudentCount: studentsCount,
        });
        reloadPage();
        setOpen(false);
    };

    return (
        <>
            <Button color="primary" onClick={handleClickOpen}>
                Edit
            </Button>
            <Dialog
                maxWidth="lg"
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Edit group</DialogTitle>
                <DialogContent>
                    <TextField
                        id="name"
                        label="Department"
                        type="text"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={department}
                        onChange={(e) => {
                            setDepartment(e.target.value);
                        }}
                    />
                    <TextField
                        id="name"
                        label="Speciality"
                        type="text"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={speciality}
                        onChange={(e) => {
                            setSpeciality(e.target.value);
                        }}
                    />
                    <TextField
                        id="add-group-number"
                        label="Count of students"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        // helperText="enter group number"
                        value={studentsCount ? studentsCount.toString() : '0'}
                        onChange={(e) => {
                            setStudentsCount(Number(e.target.value));
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={handleSaveChanges} color="primary">
                        Save changes
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default EditGroup;
