import React, { useState } from 'react';
import {
    Dialog,
    DialogActions,
    DialogTitle,
    DialogContent,
    TextField,
    Button,
} from '@material-ui/core';
import { RequestManager } from '../../request-manager';
import { Subject } from '../../interfaces/subject';

type Props = {
    reloadPage: () => void;
    subject: Subject;
};

const EditSubject = (props: Props) => {
    const { subject, reloadPage } = props;
    const [subjectNumber, setSubjectNumber] = useState<number | null>(
        () => subject.subjectNumber,
    );
    const [subjectName, setSubjectName] = useState<string>(
        () => subject.subjectName,
    );
    const [open, setOpen] = React.useState(false);

    const setStartValues = () => {
        setSubjectNumber(subject.subjectNumber);
        setSubjectName(subject.subjectName);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
        setStartValues();
    };

    const handleSaveChanges = async () => {
        await RequestManager.fetch('put', `subjects/${subject.subjectNumber}`, {
            subjectNumber: subjectNumber,
            subjectName: subjectName,
        });
        reloadPage();
        setOpen(false);
    };

    return (
        <>
            <Button color="primary" onClick={handleClickOpen}>
                Edit
            </Button>
            <Dialog
                maxWidth="lg"
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Edit subject</DialogTitle>
                <DialogContent>
                    <TextField
                        id="number"
                        label="Number"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={subjectNumber ? subjectNumber.toString() : ''}
                        onChange={(e) => {
                            setSubjectNumber(Number(e.target.value));
                        }}
                    />
                    <TextField
                        id="name"
                        label="Name"
                        type="text"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={subjectName}
                        onChange={(e) => {
                            setSubjectName(e.target.value);
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={handleSaveChanges} color="primary">
                        Save changes
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default EditSubject;
