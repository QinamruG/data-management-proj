import React from 'react';
import { Link } from 'react-router-dom';
import styles from './header.module.css';

const Header = () => {
    return (
        <div className={styles['header-container']}>
            <nav>
                <ul className={styles['tab-list']}>
                    <li className={styles['list-item']}>
                        <Link to="/">Home</Link>
                    </li>
                    <li className={styles['list-item']}>
                        <Link to="/teachers">Teachers</Link>
                    </li>
                    <li className={styles['list-item']}>
                        <Link to="/groups">Groups</Link>
                    </li>
                    <li className={styles['list-item']}>
                        <Link to="/study-loads">Study loads</Link>
                    </li>
                    <li className={styles['list-item']}>
                        <Link to="/subjects">Subjects</Link>
                    </li>
                    <li className={styles['list-item']}>
                        <Link to="/subject-payments">Subject Payments</Link>
                    </li>
                    <li className={styles['list-item']}>
                        <Link to="/search">Search</Link>
                    </li>
                    <li className={styles['list-item']}>
                        <Link to="/reports">Reports</Link>
                    </li>
                </ul>
            </nav>
        </div>
    );
};

export default Header;
