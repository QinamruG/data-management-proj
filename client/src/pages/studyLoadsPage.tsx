import React, { useEffect, useState } from 'react';
import AddStudyLoad from '../components/forms/add-study-load';
import StudyLoadsTable from '../components/tables/studyLoadsTable';
import { Group } from '../interfaces/group';
import { StudyLoad } from '../interfaces/study-load';
import { Subject } from '../interfaces/subject';
import { SubjectType } from '../interfaces/subject-type';
import { Teacher } from '../interfaces/teacher';
import { RequestManager } from '../request-manager';
import './../styles/container.css';

const StudyLoadsPage = () => {
    const [studyLoads, setStudyLoads] = useState<StudyLoad[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const [teachers, setTeachers] = useState<Teacher[]>([]);
    const [subjects, setSubjects] = useState<Subject[]>([]);
    const [subjectTypes, setSubjectTypes] = useState<SubjectType[]>([]);
    const [groups, setGroups] = useState<Group[]>([]);

    const fetchStudyLoads = async () => {
        const data = await RequestManager.getAllStudyLoads();
        if (data) await setStudyLoads(data);
        else console.log('something went wrong');
    };
    const fetchTeachers = async () => {
        const data = await RequestManager.getAllTeachers();
        if (data) setTeachers(data);
        else console.log('something went wrong with teachers');
    };
    const fetchSubjects = async () => {
        const data = await RequestManager.getAllSubjects();
        if (data) setSubjects(data);
        else console.log('something went wrong with subjects');
    };
    const fetchSubjectTypes = async () => {
        const data = await RequestManager.getAllSubjectTypes();
        if (data) setSubjectTypes(data);
        else console.log('something went wrong with subject types');
    };
    const fetchGroups = async () => {
        const data = await RequestManager.getAllGroups();
        if (data) setGroups(data);
        else console.log('something went wrong with groups');
    };

    const fetchAllData = async () => {
        await fetchStudyLoads();
        // hmm i can get in from study load
        await fetchTeachers();
        await fetchSubjects();
        await fetchSubjectTypes();
        await fetchGroups();
    };
    const reloadPage = () => {
        fetchAllData();
    };
    useEffect(() => {
        fetchAllData();
        setLoading(false);
    }, []);

    const addStudyLoadProps = {
        reloadPage: reloadPage,
        teachers: teachers,
        subjects: subjects,
        subjectTypes: subjectTypes,
        groups: groups,
    };

    const studyLoadsTableProps = {
        studyLoads: studyLoads,
        reloadPage: reloadPage,
        teachers: teachers,
        subjects: subjects,
        subjectTypes: subjectTypes,
        groups: groups,
    };

    if (loading) return <div>Loading. pls wait</div>;

    return (
        <div className="container">
            <AddStudyLoad {...addStudyLoadProps} />
            <StudyLoadsTable {...studyLoadsTableProps} />
        </div>
    );
};
export default StudyLoadsPage;
