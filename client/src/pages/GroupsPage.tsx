import React, { useEffect, useState } from 'react';
import AddGroup from '../components/forms/add-group';
import GroupsTable from '../components/tables/groupsTable';
import { Group } from '../interfaces/group';
import { RequestManager } from '../request-manager';
import './../styles/container.css';

const GroupsPage = () => {
    const [groups, setGroups] = useState<Group[]>([]);
    const [loading, setLoading] = useState<boolean>(true);

    const fetchGroups = async () => {
        const data = await RequestManager.getAllGroups();
        if (data) await setGroups(data);
        else console.log('something went wrong');
    };
    useEffect(() => {
        fetchGroups();
        setLoading(false);
    }, []);

    const reloadPage = async () => {
        fetchGroups();
    };
    const addGroupProps = {
        reloadPage: reloadPage,
    };

    const GroupsTableProps = {
        groups: groups,
        reloadPage: reloadPage,
    };

    if (loading) return <div>Loading. pls wait</div>;
    return (
        <div className="container">
            <AddGroup {...addGroupProps} />
            <GroupsTable {...GroupsTableProps} />
        </div>
    );
};

export default GroupsPage;
