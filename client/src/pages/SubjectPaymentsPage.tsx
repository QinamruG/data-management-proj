import React, { useEffect, useState } from 'react';
import AddSubjectPayment from '../components/forms/add-subject-payment';
import SubjectPaymentsTable from '../components/tables/subjectPaymentsTable';
import { Subject } from '../interfaces/subject';
import { SubjectPayment } from '../interfaces/subject-payment';
import { SubjectType } from '../interfaces/subject-type';
import { RequestManager } from '../request-manager';
import './../styles/container.css';

const SubjectPaymentsPage = () => {
    const [subjectPayments, setSubjectPayments] = useState<SubjectPayment[]>(
        [],
    );
    const [subjects, setSubjects] = useState<Subject[]>([]);
    const [subjectTypes, setSubjectTypes] = useState<SubjectType[]>([]);
    const [loading, setLoading] = useState<boolean>(true);

    const fetchSubjectPayments = async () => {
        const subjectsPayments = await RequestManager.getAllSubjectPayments();
        if (subjectsPayments) {
            await setSubjectPayments(subjectsPayments);
        } else console.log('something went wrong');
    };
    const fetchSubjects = async () => {
        const data = await RequestManager.getAllSubjects();
        if (data) setSubjects(data);
        else console.log('something went wrong with subjects');
    };
    const fetchSubjectTypes = async () => {
        const data = await RequestManager.getAllSubjectTypes();
        if (data) setSubjectTypes(data);
        else console.log('something went wrong with subject types');
    };

    const fetchAllData = async () => {
        await fetchSubjectPayments();
        await fetchSubjects();
        await fetchSubjectTypes();
    };

    const reloadPage = () => {
        fetchAllData();
    };

    useEffect(() => {
        fetchAllData();
        setLoading(false);
    }, []);

    const addSubjectPaymentProps = {
        reloadPage: reloadPage,
        subjects: subjects,
        subjectTypes: subjectTypes,
    };
    const subjectPaymentsTableProps = {
        reloadPage: reloadPage,
        subjectPayments: subjectPayments,
        subjects: subjects,
        subjectTypes: subjectTypes,
    };

    if (loading) return <div>Loading</div>;
    return (
        <div className="container">
            <AddSubjectPayment {...addSubjectPaymentProps} />
            <SubjectPaymentsTable {...subjectPaymentsTableProps} />
        </div>
    );
};
export default SubjectPaymentsPage;
