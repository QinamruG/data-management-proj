import React, { useEffect, useState } from 'react';
import AddSubject from '../components/forms/add-subject';
import AddSubjectType from '../components/forms/add-subject-type';
import SubjectsTable from '../components/tables/subjectsTable';
import SubjectTypesTable from '../components/tables/subjectTypesTable';
import { Subject } from '../interfaces/subject';
import { SubjectType } from '../interfaces/subject-type';
import { RequestManager } from '../request-manager';
import styles from './SubjectsPage.module.css';

const SubjectsPage = () => {
    const [subjects, setSubjects] = useState<Subject[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const [subjectTypes, setSubjectTypes] = useState<SubjectType[]>([]);

    const fetchSubjects = async () => {
        const subjects = await RequestManager.getAllSubjects();
        if (subjects) await setSubjects(subjects);
        else console.log('something went wrong with subjects');
    };
    const fetchSubjectTypes = async () => {
        const subjectsTypes = await RequestManager.getAllSubjectTypes();
        if (subjectsTypes) await setSubjectTypes(subjectsTypes);
        else console.log('something went wrong with subject types');
    };

    const fetchAllData = async () => {
        await fetchSubjects();
        await fetchSubjectTypes();
    };

    const reloadPage = () => {
        fetchAllData();
    };

    useEffect(() => {
        fetchAllData();
        setLoading(false);
    }, []);

    const addSubjectProps = {
        reloadPage: reloadPage,
    };

    const SubjectsTableProps = {
        subjects: subjects,
        reloadPage: reloadPage,
    };
    const addSubjectTypeProps = {
        reloadPage: reloadPage,
    };

    const SubjectTypesTableProps = {
        subjectTypes: subjectTypes,
        reloadPage: reloadPage,
    };
    if (loading) return <div>Loading</div>;
    return (
        <div className={styles['subjects-container']}>
            <div className={styles['subjects-table']}>
                <AddSubject {...addSubjectProps} />
                <SubjectsTable {...SubjectsTableProps} />
            </div>
            <div>
                <AddSubjectType {...addSubjectTypeProps} />
                <SubjectTypesTable {...SubjectTypesTableProps} />
            </div>
        </div>
    );
};
export default SubjectsPage;
