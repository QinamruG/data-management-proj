import React, { useEffect, useState } from 'react';
import AddTeacher from '../components/forms/add-teacher';
import TeachersTable from '../components/tables/teachersTable';
import { Subject } from '../interfaces/subject';
import { Teacher } from '../interfaces/teacher';
import { RequestManager } from '../request-manager';
import './../styles/container.css';

const TeachersPage = () => {
    const [teachers, setTeachers] = useState<Teacher[]>([]);
    const [subjects, setSubjects] = useState<Subject[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const fetchTeachers = async () => {
        const data = await RequestManager.getAllTeachers();
        if (data) await setTeachers(data);
        else console.log('something went wrong');
    };
    const fetchSubjects = async () => {
        const subjects = await RequestManager.getAllSubjects();
        if (subjects) await setSubjects(subjects);
        else console.log('something went wrong with subjects');
    };
    const reloadPage = () => {
        fetchTeachers();
        fetchSubjects();
    };

    useEffect(() => {
        fetchTeachers();
        fetchSubjects();
        setLoading(false);
    }, []);

    const addTeacherProps = {
        reloadPage: reloadPage,
    };
    const TeachersTableProps = {
        teachers: teachers,
        reloadPage: reloadPage,
        subjects: subjects,
    };

    if (loading) return <div>Loading. pls wait</div>;
    return (
        <div className="container">
            <AddTeacher {...addTeacherProps} />
            <TeachersTable {...TeachersTableProps} />
        </div>
    );
};
export default TeachersPage;
