import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './components/header';
import StudyLoadsPage from './pages/studyLoadsPage';
import GroupsPage from './pages/GroupsPage';
import TeachersPage from './pages/TeachersPage';
import SubjectsPage from './pages/SubjectsPage';
import SearchPage from './pages/SearchPage';
import ReportsPage from './pages/ReportsPage';
import SubjectPaymentsPage from './pages/SubjectPaymentsPage';

function App() {
    return (
        <div>
            <Router>
                <Header />
                <Switch>
                    <Route exact path="/">
                        <div>Hello there</div>
                    </Route>
                    <Route path="/teachers">
                        <TeachersPage />
                    </Route>
                    <Route path="/groups">
                        <GroupsPage />
                    </Route>
                    <Route path="/search">
                        <SearchPage />
                    </Route>
                    <Route path="/study-loads">
                        <StudyLoadsPage />
                    </Route>
                    <Route path="/subjects">
                        <SubjectsPage />
                    </Route>
                    <Route path="/reports">
                        <ReportsPage />
                    </Route>
                    <Route path="/subject-payments">
                        <SubjectPaymentsPage />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
