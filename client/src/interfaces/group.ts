export interface Group {
    groupNumber: number;
    groupSpeciality: string;
    groupDepartment: string;
    groupStudentCount: number | null;
}
