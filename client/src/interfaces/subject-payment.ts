export interface SubjectPayment {
    subjectTypeId: number;
    subjectNumber: number;
    price: number;
    studyLoad?: StudyLoad;
    subjectType?: SubjectType;
    subject?: Subject;
}

interface StudyLoad {
    teacherCode: number;
    hours: number | null;
    subjectNumber: number;
    subjectTypeId: number;
    groupNumber: number;
}

interface Subject {
    subjectNumber: number;
    subjectName: string;
}

interface SubjectType {
    subjectTypeId: number;
    type: string;
}
