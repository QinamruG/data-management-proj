export interface Subject {
    subjectNumber: number;
    subjectName: string;
    subjectPayments?: SubjectPayment[];
}

interface SubjectPayment {
    subjectTypeId: number;
    subjectNumber: number;
    price: number;
}
