export interface SubjectType {
    subjectTypeId: number;
    type: string;
    subjectPayments?: SubjectPayment[];
}

interface SubjectPayment {
    subjectTypeId: number;
    subjectNumber: number;
    price: number;
}
