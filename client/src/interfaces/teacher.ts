export interface Teacher {
    teacherCode: number;
    teacherPhone: number | null;
    teacherName: string;
    teacherSecondName: string | null;
    teacherSurname: string | null;
    teacherExperience: number | null;
    subjects?: Subject[];
    studyLoads?: StudyLoad[];
}

interface StudyLoad {
    teacherCode: number;
    hours: number | null;
    subjectNumber: number;
    subjectTypeId: number;
    groupNumber: number;
}

interface Subject {
    subjectNumber: number;
    subjectName: string;
}
