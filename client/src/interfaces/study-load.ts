import { Group } from './group';

export interface StudyLoad {
    teacherCode: number;
    hours: number | null;
    subjectNumber: number;
    subjectTypeId: number;
    groupNumber: number;
    group?: Group;
    teacher?: Teacher;
    subjectPayment?: SubjectPayment;
    subject: Subject;
    subjectType: SubjectType;
}

interface SubjectPayment {
    subjectTypeId: number;
    subjectNumber: number;
    price: number;
}

interface Teacher {
    teacherCode: number;
    teacherPhone: number | null;
    teacherName: string;
    teacherSecondName: string | null;
    teacherSurname: string | null;
    teacherExperience: number | null;
}

interface Subject {
    subjectNumber: number;
    subjectName: string;
}

interface SubjectType {
    subjectTypeId: number;
    type: string;
}
