import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
    DocumentBuilder,
    SwaggerCustomOptions,
    SwaggerModule,
} from '@nestjs/swagger';

const PORT = process.env.PORT || 3004;

async function bootstrap() {
    const app = await NestFactory.create(AppModule, { logger: true });

    app.enableCors();

    const customOptions: SwaggerCustomOptions = {
        swaggerOptions: {
            persistAuthorization: true,
            docExpansion: 'none',
        },
        customSiteTitle: 'My API Docs',
    };

    const config = new DocumentBuilder()
        .setTitle('Api of my data-managemet project')
        .addTag('QinamruG')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('/api/docs', app, document, customOptions);

    await app.listen(PORT, () => console.log(`i am alive at port ${PORT}`));
}
bootstrap();
