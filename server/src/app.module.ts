import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
    Group,
    Teacher,
    StudyLoad,
    SubjectList,
    Subject,
    SubjectType,
    SubjectPayment,
    ConcreteSubject,
    StudyLoadView,
} from './entities/index';
import { TeachersModule } from './teachers/teachers.module';
import { ConfigModule } from '@nestjs/config';
import { SubjectsModule } from './subjects/subjects.module';
import { StudyLoadsModule } from './study-loads/study-loads.module';
import { SubjectTypesModule } from './subject-types/subject-types.module';
import { GroupsModule } from './groups/groups.module';
import { SubjectPaymentsModule } from './subject-payments/subject-payments.module';
import { TeachersSubjectsModule } from './teachers-subjects/teachers-subjects.module';
import { ReportsModule } from './reports/reports.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: '.env',
        }),
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: process.env.MYSQL_DB_HOST,
            port: Number(process.env.MYSQL_DB_PORT),
            username: process.env.MYSQL_DB_USER,
            password: process.env.MYSQL_DB_PASSWORD,
            database: process.env.MYSQL_DB,
            entities: [
                Group,
                Teacher,
                StudyLoad,
                SubjectList,
                Subject,
                SubjectType,
                SubjectPayment,
                ConcreteSubject,
                StudyLoadView,
            ],
            synchronize: false,
            logging: true,
            logger: 'advanced-console',
        }),
        TeachersModule,
        SubjectsModule,
        StudyLoadsModule,
        SubjectTypesModule,
        GroupsModule,
        SubjectPaymentsModule,
        TeachersSubjectsModule,
        ReportsModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
