export class CreateGroupDto {
    groupNumber: number;

    groupSpeciality: string;

    groupDepartment: string;

    groupStudentCount: number;
}
