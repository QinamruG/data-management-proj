export class UpdateGroupDto {
    groupNumber: number;

    groupSpeciality: string;

    groupDepartment: string;

    groupStudentCount: number;
}
