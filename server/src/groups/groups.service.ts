import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Group } from 'src/entities';
import { Repository } from 'typeorm';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';

@Injectable()
export class GroupsService {
    constructor(
        @InjectRepository(Group)
        private repository: Repository<Group>,
    ) {}

    async addGroup(dto: CreateGroupDto) {
        return this.repository.insert(dto);
    }

    async getAllGroups() {
        return this.repository.find();
    }

    async getOneGroup(groupNumber: number) {
        return this.repository.findOne(groupNumber);
    }
    async removeGroup(groupNum: number) {
        return this.repository.delete(groupNum);
    }
    async updateGroup(dto: UpdateGroupDto) {
        return this.repository.save(dto);
    }
}
