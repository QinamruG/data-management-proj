import { Module } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupsController } from './groups.controller';
import { Group } from 'src/entities';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    providers: [GroupsService],
    controllers: [GroupsController],
    imports: [TypeOrmModule.forFeature([Group])],
})
export class GroupsModule {}
