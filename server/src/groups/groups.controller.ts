import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { GroupsService } from './groups.service';

@ApiTags('Groups')
@Controller('groups')
export class GroupsController {
    constructor(private service: GroupsService) {}

    @Get()
    getAll() {
        return this.service.getAllGroups();
    }

    @Get('/:id')
    getOne(@Param('id') id: number) {
        return this.service.getOneGroup(id);
    }

    @Post()
    create(@Body() dto: CreateGroupDto) {
        return this.service.addGroup(dto);
    }

    @Delete('/:groupNum')
    remove(@Param('groupNum') groupNum: number) {
        return this.service.removeGroup(groupNum);
    }
    @Put()
    change(@Body() dto: UpdateGroupDto) {
        return this.service.updateGroup(dto);
    }
}
