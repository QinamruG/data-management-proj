import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Post,
    Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateSubjectPaymentDto } from './dto/create-subject-payment.dto';
import { UpdateSubjectPaymentDto } from './dto/update-subject-payment.dto';
import { SubjectPaymentsService } from './subject-payments.service';

@ApiTags('Subject payments')
@Controller('subject-payments')
export class SubjectPaymentsController {
    constructor(private service: SubjectPaymentsService) {}

    @Get()
    getAll() {
        return this.service.getAllSubjectPayments();
    }

    @Get('subjectTypeId=:subjectTypeId&subjectNumber=:subjectNumber')
    getOne(
        @Param('subjectTypeId', ParseIntPipe) subjectTypeId: number,
        @Param('subjectNumber', ParseIntPipe) subjectNumber: number,
    ) {
        return this.service.getOnePaymentWithFullData(
            subjectTypeId,
            subjectNumber,
        );
    }

    @Post()
    create(@Body() dto: CreateSubjectPaymentDto) {
        return this.service.addSubjectPayment(dto);
    }

    @Delete('subjectTypeId=:subjectTypeId&subjectNumber=:subjectNumber')
    delete(
        @Param('subjectTypeId', ParseIntPipe) subjectTypeId: number,
        @Param('subjectNumber', ParseIntPipe) subjectNumber: number,
    ) {
        return this.service.removeOne(subjectTypeId, subjectNumber);
    }
    @Put('subjectTypeId=:subjectTypeId&subjectNumber=:subjectNumber')
    update(
        @Param('subjectTypeId', ParseIntPipe) subjectTypeId: number,
        @Param('subjectNumber', ParseIntPipe) subjectNumber: number,
        @Body() dto: UpdateSubjectPaymentDto,
    ) {
        return this.service.updateSubjectPayment(
            subjectNumber,
            subjectTypeId,
            dto,
        );
    }
}
