import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SubjectPayment } from 'src/entities';
import { Repository } from 'typeorm';
import { CreateSubjectPaymentDto } from './dto/create-subject-payment.dto';
import { UpdateSubjectPaymentDto } from './dto/update-subject-payment.dto';

@Injectable()
export class SubjectPaymentsService {
    constructor(
        @InjectRepository(SubjectPayment)
        private repository: Repository<SubjectPayment>,
    ) {}

    async addSubjectPayment(dto: CreateSubjectPaymentDto) {
        const subjectPayment = await this.repository.insert(dto);
        return subjectPayment;
    }

    getAllSubjectPayments() {
        return this.repository.find({
            relations: ['subject', 'subjectType'],
        });
    }

    async getOneSubjectPayment(subjectTypeId: number, subjectId: number) {
        const subjectPayment = await this.repository.findOne({
            where: {
                subjectTypeId: subjectTypeId,
                subjectNumber: subjectId,
            },
        });
        return subjectPayment;
    }

    async getOnePaymentWithFullData(subjectTypeId: number, subjectId: number) {
        const result = await this.repository.findOne({
            relations: ['studyLoads', 'subjectType', 'subject'],
            where: {
                subjectTypeId: subjectTypeId,
                subjectNumber: subjectId,
            },
        });
        return result;
    }

    async removeOne(subjectTypeId: number, subjectNumber: number) {
        return this.repository.delete({
            subjectTypeId: subjectTypeId,
            subjectNumber: subjectNumber,
        });
    }
    async updateSubjectPayment(
        subjectId: number,
        subjectTypeId: number,
        dto: UpdateSubjectPaymentDto,
    ) {
        return this.repository.update(
            {
                subjectTypeId: subjectTypeId,
                subjectNumber: subjectId,
            },
            dto,
        );
    }
}
