export class UpdateSubjectPaymentDto {
    subjectTypeId: number;

    subjectNumber: number;

    price: number;
}
