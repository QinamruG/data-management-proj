export class CreateSubjectPaymentDto {
    subjectTypeId: number;

    subjectNumber: number;

    price: number;
}
