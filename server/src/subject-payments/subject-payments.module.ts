import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SubjectPayment } from 'src/entities';
import { SubjectPaymentsController } from './subject-payments.controller';
import { SubjectPaymentsService } from './subject-payments.service';

@Module({
    imports: [TypeOrmModule.forFeature([SubjectPayment])],
    controllers: [SubjectPaymentsController],
    providers: [SubjectPaymentsService],
    exports: [SubjectPaymentsService],
})
export class SubjectPaymentsModule {}
