import { Module } from '@nestjs/common';
import { TeachersSubjectsService } from './teachers-subjects.service';
import { TeachersSubjectsController } from './teachers-subjects.controller';
import { SubjectList } from 'src/entities';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    providers: [TeachersSubjectsService],
    controllers: [TeachersSubjectsController],
    imports: [TypeOrmModule.forFeature([SubjectList])],
})
export class TeachersSubjectsModule {}
