import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Post,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TeacherSubjectDto } from './dto/teacher-subject.dto';
import { TeachersSubjectsService } from './teachers-subjects.service';

@ApiTags('Teachers-Subjects')
@Controller('teachers-subjects')
export class TeachersSubjectsController {
    constructor(private teachersSubjectsService: TeachersSubjectsService) {}

    @Get()
    async getAll() {
        return await this.teachersSubjectsService.getAllTeacherSubject();
    }

    @Post()
    async create(@Body() dto: TeacherSubjectDto) {
        console.log('dto', dto);
        return await this.teachersSubjectsService.createItem(dto);
    }

    @Delete('subjectNumber=:subjectNumber&teacherCode=:teacherCode')
    async delete(
        @Param('subjectNumber', ParseIntPipe) subjectNumber: number,
        @Param('teacherCode', ParseIntPipe) teacherCode: number,
    ) {
        return await this.teachersSubjectsService.removeItem(
            subjectNumber,
            teacherCode,
        );
    }
}
