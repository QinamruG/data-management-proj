export class TeacherSubjectDto {
    readonly subjectNumber: number;

    readonly teacherCode: number;
}
