import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SubjectList } from 'src/entities';
import { Repository } from 'typeorm';
import { TeacherSubjectDto } from './dto/teacher-subject.dto';

@Injectable()
export class TeachersSubjectsService {
    constructor(
        @InjectRepository(SubjectList)
        private repository: Repository<SubjectList>,
    ) {}

    async getAllTeacherSubject() {
        return await this.repository.find();
    }

    async removeItem(subjectNumber: number, teacherCode: number) {
        return await this.repository.delete({
            subjectNumber: subjectNumber,
            teacherCode: teacherCode,
        });
    }
    async createItem(dto: TeacherSubjectDto) {
        return await this.repository.insert(dto);
    }
}
