import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Post,
    Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateSubjectTypeDto } from './dto/create-subject-type.dto';
import { UpdateSubjectTypeDto } from './dto/update-subject-type.dto';
import { SubjectTypesService } from './subject-types.service';

@ApiTags('Subject types')
@Controller('subject-types')
export class SubjectTypesController {
    constructor(private service: SubjectTypesService) {}

    @Get()
    getAll() {
        return this.service.getAllSubjectTypes();
    }

    @Get('/:typeId')
    getOne(@Param('typeId', ParseIntPipe) typeId: number) {
        return this.service.getOneSubjectType(typeId);
    }

    @Get('withPayments/:typeId')
    getOneWithPayments(@Param('typeId', ParseIntPipe) typeId: number) {
        return this.service.getSubjectTypeWithPayments(typeId);
    }

    @Post()
    create(@Body() dto: CreateSubjectTypeDto) {
        return this.service.addSubjectType(dto);
    }
    @Delete(':typeId')
    remove(@Param('typeId', ParseIntPipe) typeId: number) {
        return this.service.removeByTypeId(typeId);
    }
    @Put(':typeId')
    update(
        @Param('typeId', ParseIntPipe) typeId: number,
        @Body() dto: UpdateSubjectTypeDto,
    ) {
        return this.service.updateByTypeId(typeId, dto);
    }
}
