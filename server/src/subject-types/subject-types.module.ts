import { Module } from '@nestjs/common';
import { SubjectTypesService } from './subject-types.service';
import { SubjectTypesController } from './subject-types.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SubjectType } from 'src/entities';

@Module({
    providers: [SubjectTypesService],
    controllers: [SubjectTypesController],
    imports: [TypeOrmModule.forFeature([SubjectType])],
})
export class SubjectTypesModule {}
