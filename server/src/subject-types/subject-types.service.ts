import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SubjectType } from 'src/entities';
import { Repository } from 'typeorm';
import { CreateSubjectTypeDto } from './dto/create-subject-type.dto';
import { UpdateSubjectTypeDto } from './dto/update-subject-type.dto';

@Injectable()
export class SubjectTypesService {
    constructor(
        @InjectRepository(SubjectType)
        private repository: Repository<SubjectType>,
    ) {}

    async addSubjectType(dto: CreateSubjectTypeDto) {
        const subject = await this.repository.insert(dto);
        return subject;
    }

    async getAllSubjectTypes() {
        return this.repository.find();
    }

    async getOneSubjectType(id: number) {
        return this.repository.findOne(id);
    }
    async getSubjectTypeWithPayments(id: number) {
        return this.repository.findOne({
            relations: ['subjectPayments'],
            where: {
                subjectTypeId: id,
            },
        });
    }
    async removeByTypeId(typeId: number) {
        return this.repository.delete(typeId);
    }
    async updateByTypeId(typeId: number, dto: UpdateSubjectTypeDto) {
        return this.repository.update(typeId, dto);
    }
}
