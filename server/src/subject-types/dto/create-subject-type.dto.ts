export class CreateSubjectTypeDto {
    readonly subjectTypeId: number;

    readonly type: string;
}
