export class UpdateSubjectTypeDto {
    readonly subjectTypeId: number;

    readonly type: string;
}
