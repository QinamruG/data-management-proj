import PdfPrinter from 'pdfmake';
import path from 'path';
import fs from 'fs';
import { pdfTemplate } from './templates/pdfTemplate';

export type Input = { [key: string]: string };

const fonts = {
    Roboto: {
        normal: 'fonts/Roboto-Regular.ttf',
        light: 'fonts/Roboto-Light.ttf',
        bold: 'fonts/Roboto-Medium.ttf',
        italics: 'fonts/Roboto-Italic.ttf',
        bolditalics: 'fonts/Roboto-MediumItalic.ttf',
    },
};

class PdfGenerator {
    private count: number;

    constructor(private pdfPrinter: PdfPrinter) {
        this.count = 1;
    }

    async generatePdfDoc(pdfDoc, callback) {
        const doc = this.pdfPrinter.createPdfKitDocument(pdfDoc);
        const chunks = [];

        doc.on('data', function (chunk) {
            chunks.push(chunk);
        });
        doc.on('end', function () {
            const buffer = Buffer.concat(chunks);
            callback(
                'data:application/pdf;base64,' + buffer.toString('base64'),
            );
        });
        doc.pipe(fs.createWriteStream('pdfs/absolute.pdf'));
        doc.end();
        return doc;
    }

    async savePdfFile(pdf) {
        fs.writeFile(
            path.resolve('./../../public', `reports/${this.count}.pdf`),
            pdf,
            (err) => {
                if (err) console.error(err);
                console.log('file was saved');
            },
        );
    }

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    async getPdf(inputs: Input[]) {}
}

// export const pdfGenerator = new PdfGenerator(pdfTemplate);
/*
(async () => {
    // You can also use Uint8Array or ArrayBuffer as a basePdf
    // const basePdf = fs.readFileSync("path/to/your.pdf")
    // const basePdf = await fetch("path/to/your.pdf").then((res) => res.arrayBuffer());
    const template = {
        basePdf: {},
        schemas: [
            {
                header: {
                    type: 'text',
                    position: {
                        x: 20,
                        y: 20,
                    },
                    width: 99.46,
                    height: 15,
                    alignment: 'center',
                    fontSize: 30,
                    characterSpacing: 0,
                    lineHeight: 1,
                },
                field1: {
                    type: 'text',
                    position: {
                        x: 20,
                        y: 35,
                    },
                    width: 99.2,
                    height: 22.27,
                    alignment: 'left',
                    fontSize: 20,
                    characterSpacing: 0,
                    lineHeight: 1,
                },
                field2: {
                    type: 'text',
                    position: {
                        x: 19.84,
                        y: 57.15,
                    },
                    width: 99.56,
                    height: 16.52,
                    alignment: 'left',
                    fontSize: 12,
                    characterSpacing: 0,
                    lineHeight: 1,
                },
            },
        ],
    };
    const inputs = [{ header: 'Teachers', field1: 'abiba', field2: 'boba' }];
    const pdf = await labelmake({ template, inputs });
    // Node
    fs.writeFileSync('path/to/your.pdf', pdf);
    // Browser
    // const blob = new Blob([pdf.buffer], { type: "application/pdf" });
    // document.getElementById("iframe").src = URL.createObjectURL(blob);
})();
*/
