export const pdfTemplate = {
    basePdf: { width: 210, height: 297 },
    schemas: [
        {
            header: {
                position: { x: 20, y: 20 },
                width: 50,
                height: 50,
                fontSize: 30,
                type: 'text',
            },
            field1: {
                position: { x: 20, y: 35 },
                width: 50,
                height: 50,
                fontSize: 20,
                type: 'text',
            },
            field2: {
                position: { x: 20, y: 35 },
                width: 50,
                height: 50,
                fontSize: 20,
                type: 'text',
            },
        },
    ],
};
