import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Post,
    Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateStudyLoadDto } from './dto/create-study-load.dto';
import { UpdateStudyLoadDto } from './dto/update-study-load.dto';
import { StudyLoadsService } from './study-loads.service';

@ApiTags('Study loads')
@Controller('study-loads')
export class StudyLoadsController {
    constructor(private service: StudyLoadsService) {}

    @Get()
    getAll() {
        return this.service.getAllStudyLoads();
    }

    @Get(
        'teacherCode=:teacherCode&subjectNumber=:subjectNumber&subjectTypeId=:subjectTypeId&groupNumber=:groupNumber',
    )
    getOne(
        @Param('teacherCode', ParseIntPipe) teacherCode: number,
        @Param('subjectNumber', ParseIntPipe) subjectNumber: number,
        @Param('subjectTypeId', ParseIntPipe) subjectTypeId: number,
        @Param('groupNumber', ParseIntPipe) groupNumber: number,
    ) {
        return this.service.getOneStudyLoad(
            teacherCode,
            subjectNumber,
            subjectTypeId,
            groupNumber,
        );
    }

    @Put()
    change(@Body() dto: UpdateStudyLoadDto) {
        return this.service.updateStudyLoad(dto);
    }

    @Post()
    create(@Body() dto: CreateStudyLoadDto) {
        return this.service.addStudyLoad(dto);
    }

    @Delete(
        'teacherCode=:teacherCode&subjectNumber=:subjectNumber&subjectTypeId=:subjectTypeId&groupNumber=:groupNumber',
    )
    remove(
        @Param('teacherCode', ParseIntPipe) teacherCode: number,
        @Param('subjectNumber', ParseIntPipe) subjectNumber: number,
        @Param('subjectTypeId', ParseIntPipe) subjectTypeId: number,
        @Param('groupNumber', ParseIntPipe) groupNumber: number,
    ) {
        return this.service.removeOne(
            teacherCode,
            subjectNumber,
            subjectTypeId,
            groupNumber,
        );
    }
}
