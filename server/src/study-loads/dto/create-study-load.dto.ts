export class CreateStudyLoadDto {
    teacherCode: number;

    hours: number;

    subjectNumber: number;

    subjectTypeId: number;

    groupNumber: number;
}
