export class UpdateStudyLoadDto {
    teacherCode: number;

    hours: number;

    subjectNumber: number;

    subjectTypeId: number;

    groupNumber: number;
}
