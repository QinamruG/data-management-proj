import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudyLoad } from 'src/entities';
import { StudyLoadsController } from './study-loads.controller';
import { StudyLoadsService } from './study-loads.service';

@Module({
    providers: [StudyLoadsService],
    controllers: [StudyLoadsController],
    imports: [TypeOrmModule.forFeature([StudyLoad])],
    exports: [StudyLoadsService],
})
export class StudyLoadsModule {}
