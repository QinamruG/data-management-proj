import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StudyLoad } from 'src/entities';
import { Repository } from 'typeorm';
import { CreateStudyLoadDto } from './dto/create-study-load.dto';

@Injectable()
export class StudyLoadsService {
    constructor(
        @InjectRepository(StudyLoad)
        private repository: Repository<StudyLoad>,
    ) {}

    async addStudyLoad(dto: CreateStudyLoadDto) {
        const studyLoad = await this.repository.insert(dto);
        return studyLoad;
    }

    async getAllStudyLoads() {
        return this.repository.find({
            relations: [
                'group',
                'teacher',
                'subjectPayment',
                'subject',
                'subjectType',
            ],
        });
    }

    async getOneStudyLoad(tc, subjNum, stid, grNum) {
        return this.repository.findOne({
            relations: ['group', 'teacher', 'subjectPayment'],
            where: {
                teacherCode: tc,
                subjectNumber: subjNum,
                subjectTypeId: stid,
                groupNumber: grNum,
            },
        });
    }

    async removeOne(tc, subjNum, stid, grNum) {
        return this.repository.delete({
            teacherCode: tc,

            subjectNumber: subjNum,

            subjectTypeId: stid,

            groupNumber: grNum,
        });
    }

    async updateStudyLoad(dto) {
        return this.repository.save(dto);
    }

    async getStudyLoadsByTeacher(teacherCode) {
        return this.repository.find({
            where: {
                teacherCode: teacherCode,
            },
        });
    }
}
