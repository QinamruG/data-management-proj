import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToMany,
    JoinTable,
    OneToMany,
} from 'typeorm';
import { StudyLoad } from './study-load.entity';
import { Subject } from './subject.entity';

@Entity({ name: 'Teacher' })
export class Teacher {
    @PrimaryGeneratedColumn({ type: 'int' })
    teacherCode: number;

    @Column({ type: 'int', width: 60, nullable: true })
    teacherPhone: number;

    @Column({ type: 'varchar', length: 25 })
    teacherName: string;

    @Column({ type: 'varchar', length: 25, nullable: true })
    teacherSecondName: string;

    @Column({ type: 'varchar', length: 25, nullable: true })
    teacherSurname: string;

    @Column({ type: 'int', width: 60, nullable: true })
    teacherExperience: number;

    @ManyToMany(() => Subject)
    @JoinTable({
        name: 'SubjectList',
        joinColumn: {
            name: 'teacherCode',
            referencedColumnName: 'teacherCode',
        },
        inverseJoinColumn: {
            name: 'subjectNumber',
            referencedColumnName: 'subjectNumber',
        },
    })
    subjects: Subject[];

    @OneToMany(() => StudyLoad, (studyLoad) => studyLoad.teacher)
    studyLoads: StudyLoad[];
}
