import { Entity, Column, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Group } from './group.entity';
import { SubjectPayment } from './subject-payment.entity';
import { SubjectType } from './subject-type.entity';
import { Subject } from './subject.entity';
import { Teacher } from './teacher.entity';

@Entity({ name: 'StudyLoad' })
export class StudyLoad {
    @PrimaryColumn({ type: 'int', width: 60 })
    teacherCode: number;

    @Column({ type: 'int', width: 60, nullable: true })
    hours: number;

    @PrimaryColumn({ type: 'int', width: 60 })
    subjectNumber: number;

    @PrimaryColumn({ type: 'int', width: 60 })
    subjectTypeId: number;

    @PrimaryColumn({ type: 'int', width: 60 })
    groupNumber: number;

    @ManyToOne(() => Group, (group) => group.studyLoads)
    @JoinColumn({
        name: 'groupNumber',
        referencedColumnName: 'groupNumber',
    })
    group: Group;

    @ManyToOne(() => Teacher, (teacher) => teacher.studyLoads)
    @JoinColumn({
        name: 'teacherCode',
        referencedColumnName: 'teacherCode',
    })
    teacher: Teacher;

    @ManyToOne(() => SubjectPayment, (sp) => sp.studyLoads)
    @JoinColumn([
        {
            name: 'subjectNumber',
            referencedColumnName: 'subjectNumber',
        },
        {
            name: 'subjectTypeId',
            referencedColumnName: 'subjectTypeId',
        },
    ])
    subjectPayment: SubjectPayment;

    @ManyToOne(() => Subject, (subject) => subject.studyLoads)
    @JoinColumn({
        name: 'subjectNumber',
        referencedColumnName: 'subjectNumber',
    })
    subject: Subject;

    @ManyToOne(() => SubjectType, (subjectType) => subjectType.studyLoads)
    @JoinColumn({
        name: 'subjectTypeId',
        referencedColumnName: 'subjectTypeId',
    })
    subjectType: SubjectType;
}
