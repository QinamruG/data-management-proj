import { Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'SubjectList' })
export class SubjectList {
    @PrimaryColumn({ type: 'int', width: 60 })
    subjectNumber: number;

    @PrimaryColumn({ type: 'int', width: 60 })
    teacherCode: number;
}
