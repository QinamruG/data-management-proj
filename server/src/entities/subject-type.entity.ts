import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { StudyLoad } from './study-load.entity';
import { SubjectPayment } from './subject-payment.entity';

@Entity({ name: 'SubjectType' })
export class SubjectType {
    @PrimaryGeneratedColumn({ type: 'int' })
    subjectTypeId: number;

    @Column({ type: 'varchar', length: 20 })
    type: string;

    @OneToMany(
        () => SubjectPayment,
        (subjectPayment) => subjectPayment.subjectType,
    )
    subjectPayments: SubjectPayment[];

    @OneToMany(() => StudyLoad, (studyLoad) => studyLoad.subjectTypeId)
    studyLoads: StudyLoad[];
}
