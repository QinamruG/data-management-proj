import { ViewEntity, ViewColumn } from 'typeorm';

@ViewEntity({
    expression: `select \`Group\`.\`groupNumber\` AS \`groupNumber\`,\`Teacher\`.\`teacherName\` AS \`teacherName\`,\`ConcreteSubject\`.\`subjectName\` AS \`subjectName\`,\`ConcreteSubject\`.\`type\` AS \`type\`,\`ConcreteSubject\`.\`price\` AS \`price\`,\`StudyLoad\`.\`hours\` AS \`hours\` from (((\`Group\` join \`StudyLoad\`) join \`Teacher\`) join \`ConcreteSubject\`)`,
})
export class StudyLoadView {
    @ViewColumn()
    teacherName: string;

    @ViewColumn()
    hours: number;

    @ViewColumn()
    subjectName: string;

    @ViewColumn()
    type: string;

    @ViewColumn()
    groupNumber: number;

    @ViewColumn()
    price: number;
}

// select `Group`.`groupNumber` AS `groupNumber`,`Teacher`.`teacherName` AS `teacherName`,`ConcreteSubject`.`subjectName` AS `subjectName`,`ConcreteSubject`.`type` AS `type`,`ConcreteSubject`.`price` AS `price`,`StudyLoad`.`hours` AS `hours` from (((`Group` join `StudyLoad`) join `Teacher`) join `ConcreteSubject`)
