import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { StudyLoad } from './study-load.entity';

@Entity({ name: 'Group' })
export class Group {
    @PrimaryColumn({ type: 'int', width: 60 })
    groupNumber: number;

    @Column({ type: 'varchar', length: 50 })
    groupSpeciality: string;

    @Column({ type: 'varchar', length: 50 })
    groupDepartment: string;

    @Column({ type: 'int', width: 60, nullable: true })
    groupStudentCount: number;

    @OneToMany(() => StudyLoad, (studyLoad) => studyLoad.group)
    studyLoads: StudyLoad[];
}
