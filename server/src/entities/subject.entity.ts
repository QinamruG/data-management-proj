import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { StudyLoad } from './study-load.entity';
import { SubjectPayment } from './subject-payment.entity';

@Entity({ name: 'Subject' })
export class Subject {
    @PrimaryGeneratedColumn({ type: 'int' })
    subjectNumber: number;

    @Column({ type: 'varchar', length: 35 })
    subjectName: string;

    @OneToMany(
        () => SubjectPayment,
        (subjectPayment) => subjectPayment.subjectNumber,
    )
    subjectPayments: SubjectPayment[];

    @OneToMany(() => StudyLoad, (studyLoad) => studyLoad.subjectNumber)
    studyLoads: StudyLoad[];
}
