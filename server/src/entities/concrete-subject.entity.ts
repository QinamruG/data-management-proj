import { ViewEntity, ViewColumn } from 'typeorm';

@ViewEntity({
    expression: `select \`Subject\`.\`subjectName\` AS \`subjectName\`,\`SubjectType\`.\`type\` AS \`type\`,\`SubjectPayment\`.\`price\` AS \`price\` from ((\`SubjectPayment\` join \`SubjectType\`) join \`Subject\`)`,
})
export class ConcreteSubject {
    @ViewColumn()
    subjectName: string;

    @ViewColumn()
    type: string;

    @ViewColumn()
    price: number;
}
// select `Subject`.`subjectName` AS `subjectName`,`SubjectType`.`type` AS `type`,`SubjectPayment`.`price` AS `price` from ((`SubjectPayment` join `SubjectType`) join `Subject`)
