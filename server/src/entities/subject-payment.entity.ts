import {
    Entity,
    Column,
    PrimaryColumn,
    JoinColumn,
    ManyToOne,
    OneToMany,
} from 'typeorm';
import { StudyLoad } from './study-load.entity';
import { Subject, SubjectType } from './index';

@Entity({ name: 'SubjectPayment' })
export class SubjectPayment {
    @PrimaryColumn({ type: 'int', width: 60 })
    subjectTypeId: number;

    @PrimaryColumn({ type: 'int', width: 60 })
    subjectNumber: number;

    @Column({ type: 'int', width: 60 })
    price: number;

    @OneToMany(() => StudyLoad, (sl) => sl.subjectPayment)
    @JoinColumn([
        {
            name: 'subjectNumber',
            referencedColumnName: 'subjectNumber',
        },
        {
            name: 'subjectTypeId',
            referencedColumnName: 'subjectTypeId',
        },
    ])
    studyLoads: StudyLoad[];

    @ManyToOne(() => Subject, (subject) => subject.subjectNumber)
    @JoinColumn({
        name: 'subjectNumber',
        referencedColumnName: 'subjectNumber',
    })
    subject: Subject;

    @ManyToOne(() => SubjectType, (subjectType) => subjectType.subjectTypeId)
    @JoinColumn({
        name: 'subjectTypeId',
        referencedColumnName: 'subjectTypeId',
    })
    subjectType: SubjectType;
}
