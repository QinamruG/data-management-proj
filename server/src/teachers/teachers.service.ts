import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Teacher } from 'src/entities';
import { StudyLoadsService } from 'src/study-loads/study-loads.service';
import { Repository } from 'typeorm';
import { CreateTeacherDto } from './dto/create-teacher.dto';

@Injectable()
export class TeachersService {
    constructor(
        @InjectRepository(Teacher)
        private repository: Repository<Teacher>,
        private studyLoadsService: StudyLoadsService,
    ) {}

    async addTeacher(dto: CreateTeacherDto) {
        const teacher = await this.repository.insert(dto);
        return teacher;
    }

    async getAllTeachers() {
        return this.repository.find({
            relations: ['subjects'],
        });
    }

    async getAllTeachersWithSubjects() {
        return this.repository.find({
            relations: ['subjects'],
        });
    }

    async getOneTeacher(teacherCode: number) {
        return this.repository.findOne({
            relations: ['subjects', 'studyLoads'],
            where: {
                teacherCode: teacherCode,
            },
        });
    }

    async updateTeacher(teacherCode: number, dto: CreateTeacherDto) {
        return this.repository.update(teacherCode, dto);
    }

    async getTeacherWithFullInfo(teacherCode: number) {
        const teacher = await this.repository.findOne({
            relations: ['subjects', 'studyLoads'],
            where: {
                teacherCode: teacherCode,
            },
        });
        return teacher;
    }

    async removeTeacher(teacherCode: number) {
        const isTeachersHasThisSubject = (
            subjectNum: number,
            teachers: Teacher[],
        ): boolean => {
            for (const teacher of teachers) {
                if (
                    teacher.subjects.some(
                        (subj) => subj.subjectNumber === subjectNum,
                    )
                )
                    return true;
            }
            return false;
        };

        const studyLoads = await this.studyLoadsService.getStudyLoadsByTeacher(
            teacherCode,
        );

        const subjectNumbers = Array.from(
            new Set(studyLoads.map((sl) => sl.subjectNumber)),
        );

        const teachersWithSubjects = (
            await this.getAllTeachersWithSubjects()
        ).filter((teacher) => teacher.teacherCode !== teacherCode);

        for (const subjectNum of subjectNumbers) {
            if (isTeachersHasThisSubject(subjectNum, teachersWithSubjects))
                continue;
            else return 'no teachers for replace';
        }

        await this.repository.query('CALL change_teacher_in_study_loads(?)', [
            teacherCode,
        ]);

        return this.repository.delete(teacherCode);
    }
}
