import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Post,
    Put,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Teacher } from 'src/entities';
import { CreateTeacherDto } from './dto/create-teacher.dto';
import { TeachersService } from './teachers.service';

@ApiTags('Teachers')
@Controller('/teachers')
export class TeachersController {
    constructor(private teachersService: TeachersService) {}

    @Get()
    getAll() {
        return this.teachersService.getAllTeachers();
    }

    @Get('/:teacherCode')
    getOne(@Param('teacherCode', ParseIntPipe) teacherCode: number) {
        return this.teachersService.getOneTeacher(teacherCode);
    }

    @ApiResponse({ status: 200, type: Teacher })
    @Post()
    create(@Body() teacherDto: CreateTeacherDto) {
        return this.teachersService.addTeacher(teacherDto);
    }

    @Put('/:teacherCode')
    update(
        @Param('teacherCode', ParseIntPipe) teacherCode: number,
        @Body() dto: CreateTeacherDto,
    ) {
        return this.teachersService.updateTeacher(teacherCode, dto);
    }

    @Get('full/:teacherCode')
    GetOneFull(@Param('teacherCode', ParseIntPipe) teacherCode: number) {
        return this.teachersService.getTeacherWithFullInfo(teacherCode);
    }

    @Delete('/:teacherCode')
    async delete(@Param('teacherCode', ParseIntPipe) teacherCode: number) {
        return await this.teachersService.removeTeacher(teacherCode);
    }
}
