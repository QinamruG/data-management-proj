import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudyLoadsModule } from 'src/study-loads/study-loads.module';
import { SubjectsModule } from 'src/subjects/subjects.module';
import { Teacher } from '../entities/index';
import { TeachersController } from './teachers.controller';
import { TeachersService } from './teachers.service';
@Module({
    providers: [TeachersService],
    controllers: [TeachersController],
    imports: [
        TypeOrmModule.forFeature([Teacher]),
        SubjectsModule,
        StudyLoadsModule,
    ],
})
export class TeachersModule {}
