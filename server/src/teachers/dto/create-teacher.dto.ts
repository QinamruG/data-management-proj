export class CreateTeacherDto {
    readonly teacherCode?: number;

    readonly teacherName: string;

    readonly teacherSecondName?: string;

    readonly teacherSurname?: string;

    readonly teacherPhone?: number;

    readonly teacherExperience?: number;
}
