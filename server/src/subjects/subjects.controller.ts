import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Post,
    Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateSubjectDto } from './dto/create-subject.dto';
import { UpdateSubjectDto } from './dto/update-subject.dto';
import { SubjectsService } from './subjects.service';

@ApiTags('Subjects')
@Controller('subjects')
export class SubjectsController {
    constructor(private service: SubjectsService) {}

    @Get()
    getAll() {
        return this.service.getAllSubjects();
    }

    @Get('/:subjectNumber')
    getOne(@Param('subjectNumber', ParseIntPipe) subjectNumber: number) {
        return this.service.getOneSubject(subjectNumber);
    }

    @Get('withPayments/:subjectNumber')
    getOneWithPayments(
        @Param('subjectNumber', ParseIntPipe) subjectNumber: number,
    ) {
        return this.service.getOneWithPayments(subjectNumber);
    }

    @Post()
    create(@Body() dto: CreateSubjectDto) {
        return this.service.addSubject(dto);
    }

    @Delete('/:subjectNumber')
    delete(@Param('subjectNumber', ParseIntPipe) subjectNumber: number) {
        return this.service.removeOne(subjectNumber);
    }
    @Put(':subjectNumber')
    update(
        @Param('subjectNumber', ParseIntPipe) subjectNumber: number,
        @Body() dto: UpdateSubjectDto,
    ) {
        return this.service.updateByNumber(subjectNumber, dto);
    }
}
