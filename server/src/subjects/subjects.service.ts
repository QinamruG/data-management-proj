import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Subject } from 'src/entities';
import { Repository } from 'typeorm';
import { CreateSubjectDto } from './dto/create-subject.dto';
import { UpdateSubjectDto } from './dto/update-subject.dto';

@Injectable()
export class SubjectsService {
    constructor(
        @InjectRepository(Subject)
        private repository: Repository<Subject>,
    ) {}

    async addSubject(dto: CreateSubjectDto) {
        const subject = await this.repository.insert(dto);
        return subject;
    }

    async getAllSubjects() {
        return this.repository.find();
    }

    async getOneSubject(subjectNumber: number) {
        return this.repository.findOne(subjectNumber);
    }

    async getOneWithPayments(subjectNumber: number) {
        return this.repository.findOne({
            relations: ['subjectPayments'],
            where: {
                subjectNumber: subjectNumber,
            },
        });
    }

    async removeOne(subjectNumber: number) {
        return this.repository.delete(subjectNumber);
    }

    async updateByNumber(subjectNumber: number, dto: UpdateSubjectDto) {
        return this.repository.update(subjectNumber, dto);
    }
}
