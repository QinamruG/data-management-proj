export class CreateSubjectDto {
    readonly subjectNumber: number;

    readonly subjectName: string;
}
