export class UpdateSubjectDto {
    readonly subjectNumber: number;

    readonly subjectName: string;
}
