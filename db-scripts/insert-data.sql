-- Adminer 4.7.8 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `Group` (`groupNumber`, `groupSpeciality`, `groupDepartment`, `groupStudentCount`) VALUES
(167,	'Электроника и наноэлектроника',	'ТУиЦИ',	25),
(187,	'Асфальтоукладка',	'ДУАИ',	13),
(229,	'Прикладная математика',	'ВДДО',	40),
(321,	'Интернет-маркетинг',	'ТУиЦИ',	15),
(334,	'Физика',	'ФНХиТ',	20),
(335,	'Органика',	'ФОХиТ',	23),
(342,	'Информационные системы и технологии',	'ТУиЦИ',	25),
(2185,	'Анализ данных',	'ТУиЦИ',	20),
(3147,	'Информационные системы и технологии',	'ТУиЦИ',	30),
(4186,	'Кибер безопасность',	'ТУиЦИ',	10);

INSERT INTO `Subject` (`subjectNumber`, `subjectName`) VALUES
(1,	'Управление данными'),
(2,	'Биология'),
(3,	'Химия'),
(4,	'Английский язык'),
(5,	'Мат. анализ'),
(6,	'Схемотехника'),
(7,	'АИС'),
(8,	'Физика'),
(9,	'Технологии программирования'),
(10,	'Информатика');

INSERT INTO `SubjectList` (`teacherCode`, `subjectNumber`) VALUES
(1, 1),
(1,	2),
(1,	7),
(2, 5),
(2,	6),
(3,	3),
(3,	9),
(4,	3),
(4,	10),
(4, 6),
(4, 1),
(5,	2),
(5, 10),
(6,	4),
(7,	2),
(7,	8),
(8,	4),
(9,	3),
(9,	5),
(10, 2),
(10, 5);

INSERT INTO `SubjectPayment` (`price`, `subjectNumber`, `subjectTypeId`) VALUES
(100,	1,	1),
(110,	1,	2),
(90,	2,	3),
(100,	3,	1),
(130,	3,	2),
(100,	4,	4),
(90,	5,	1),
(120,	6,	3),
(100,	7,	1),
(120,	7,	4),
(100,	8,	2),
(120,	9,	1),
(80,	10,	2),
(100,	10,	3);

INSERT INTO `SubjectType` (`subjectTypeId`, `type`) VALUES
(1,	'Лекция'),
(2,	'Лабораторная работа'),
(3,	'Семинар'),
(4,	'Практическое занятие');

INSERT INTO `Teacher` (`teacherCode`, `teacherPhone`, `teacherSurname`, `teacherName`, `teacherSecondName`, `teacherExperience`) VALUES
(1,	NULL,	'Марков',	'Егор',	NULL,	4),
(2,	2147483647,	'Иванов',	'Андрей',	'Якубович',	10),
(3,	2147483647,	'Чернышев',	'Артем',	'Сергеевич',	5),
(4,	363636,	'Осипов',	'Александр',	'Сергеевич',	6),
(5,	919234,	'Тестов',	'Вводилен',	'Даннович',	1),
(6,	111,	'Куроборец',	'Денис',	'Бублинович',	2),
(7,	2147483,	'Иванков',	'Андрей',	'Убович',	12),
(8,	2147483,	'Иванков',	'Иван',	'Иванович',	7),
(9,	2147483471,	'Иванкertов',	'AH',	'123',	1),
(10,	NULL,	NULL,	'Иван',	NULL,	2);

INSERT INTO `StudyLoad` (`teacherCode`, `hours`, `subjectNumber`, `subjectTypeId`, `groupNumber`) VALUES
(1,	2,	1,	1,	342),
(1,	2,	2,	3,	335),
(2,	2,	5,	1,	2185),
(1,	2,	7,	1,	3147),
(1,	2,	7,	4,	3147),
(3,	2,	3,	1,	229),
(6,	2,	4,	4,	321),
(4,	2,	1,	2,	342),
(4,	4,	6,	3,	167),
(4,	4,	6,	3,	3147),
(7,	8,	8,	2,	187),
(3,	2,	9,	1,	3147),
(3,	2,	9,	1,	4186),
(5,	1,	10,	3,	334);

-- 2021-05-31 14:38:19
