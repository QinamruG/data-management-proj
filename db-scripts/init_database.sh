#!/bin/sh
DB_USER=root
DB_PASS=root

cat \
    /db-scripts/create-tables.sql \
    /db-scripts/create-routines.sql \
    /db-scripts/insert-data.sql |
    mysql -u $DB_USER --password=$DB_PASS -v --default-character-set=utf8mb4
