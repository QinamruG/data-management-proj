-- Adminer 4.7.8 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;

DROP DATABASE IF EXISTS `load`;
CREATE DATABASE `load` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `load`;

CREATE TABLE `ConcreteSubject` (`subjectName` varchar(35), `type` varchar(20), `price` int(60));


CREATE TABLE `Group` (
  `groupNumber` int(60) NOT NULL,
  `groupSpeciality` varchar(50) NOT NULL,
  `groupDepartment` varchar(50) NOT NULL,
  `groupStudentCount` int(60) DEFAULT NULL,
  PRIMARY KEY (`groupNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `StudyLoad` (
  `teacherCode` int(60) NOT NULL,
  `hours` int(60) DEFAULT NULL,
  `subjectNumber` int(60) NOT NULL,
  `subjectTypeId` int(60) NOT NULL,
  `groupNumber` int(60) NOT NULL,
  PRIMARY KEY (`teacherCode`,`subjectNumber`,`subjectTypeId`,`groupNumber`),
  KEY `R_5` (`groupNumber`),
  KEY `R_24` (`subjectNumber`,`subjectTypeId`),
  CONSTRAINT `StudyLoad_ibfk_6` FOREIGN KEY (`teacherCode`) REFERENCES `Teacher` (`teacherCode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `StudyLoad_ibfk_7` FOREIGN KEY (`groupNumber`) REFERENCES `Group` (`groupNumber`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `StudyLoad_ibfk_8` FOREIGN KEY (`subjectNumber`, `subjectTypeId`) REFERENCES `SubjectPayment` (`subjectNumber`, `subjectTypeId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;

CREATE TRIGGER `StudyLoad_check_teacher_competencies` BEFORE INSERT ON `StudyLoad` FOR EACH ROW
BEGIN
    IF NEW.subjectNumber NOT IN
    (SELECT sl.subjectNumber FROM Teacher t
    JOIN SubjectList sl ON sl.teacherCode = t.teacherCode
    where sl.teacherCode = NEW.teacherCode)

    THEN    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Teacher doesnt have this subject in competencies';
    END IF;
END;;

DELIMITER ;

CREATE TABLE `StudyLoadView` (`groupNumber` int(60), `teacherName` varchar(25), `subjectName` varchar(35), `type` varchar(20), `price` int(60), `hours` int(60));


CREATE TABLE `Subject` (
  `subjectNumber` int(60) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(35) NOT NULL,
  PRIMARY KEY (`subjectNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `SubjectList` (
  `subjectNumber` int(60) NOT NULL,
  `teacherCode` int(60) NOT NULL,
  PRIMARY KEY (`teacherCode`,`subjectNumber`),
  KEY `R_22` (`subjectNumber`),
  KEY `R_9` (`teacherCode`),
  CONSTRAINT `SubjectList_ibfk_3` FOREIGN KEY (`teacherCode`) REFERENCES `Teacher` (`teacherCode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SubjectList_ibfk_4` FOREIGN KEY (`subjectNumber`) REFERENCES `Subject` (`subjectNumber`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;

CREATE TRIGGER `SubjectList_bd` BEFORE DELETE ON `SubjectList` FOR EACH ROW
begin
    if exists (select * from SubjectList sl
    where sl.subjectNumber = OLD.subjectNumber 
    and sl.teacherCode != OLD.teacherCode)
    then call changeTeacherInStudyLoadsWithThatSubject(OLD.subjectNumber, OLD.teacherCode);
    else SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'This subject cant be raplaced from competencies';
    end if;
end;;

DELIMITER ;

CREATE TABLE `SubjectPayment` (
  `price` int(60) NOT NULL,
  `subjectNumber` int(60) NOT NULL,
  `subjectTypeId` int(60) NOT NULL,
  PRIMARY KEY (`subjectNumber`,`subjectTypeId`),
  KEY `R_7` (`subjectNumber`),
  KEY `R_10` (`subjectTypeId`),
  CONSTRAINT `SubjectPayment_ibfk_1` FOREIGN KEY (`subjectNumber`) REFERENCES `Subject` (`subjectNumber`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `SubjectPayment_ibfk_2` FOREIGN KEY (`subjectTypeId`) REFERENCES `SubjectType` (`subjectTypeId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `SubjectType` (
  `subjectTypeId` int(60) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`subjectTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `Teacher` (
  `teacherCode` int(60) NOT NULL AUTO_INCREMENT,
  `teacherPhone` int(60) DEFAULT NULL,
  `teacherSurname` varchar(25) DEFAULT NULL,
  `teacherName` varchar(25) NOT NULL,
  `teacherSecondName` varchar(25) DEFAULT NULL,
  `teacherExperience` int(60) DEFAULT NULL,
  PRIMARY KEY (`teacherCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `ConcreteSubject`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `ConcreteSubject` AS select `Subject`.`subjectName` AS `subjectName`,`SubjectType`.`type` AS `type`,`SubjectPayment`.`price` AS `price` from ((`SubjectPayment` join `SubjectType`) join `Subject`);

DROP TABLE IF EXISTS `StudyLoadView`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `StudyLoadView` AS select `Group`.`groupNumber` AS `groupNumber`,`Teacher`.`teacherName` AS `teacherName`,`ConcreteSubject`.`subjectName` AS `subjectName`,`ConcreteSubject`.`type` AS `type`,`ConcreteSubject`.`price` AS `price`,`StudyLoad`.`hours` AS `hours` from (((`Group` join `StudyLoad`) join `Teacher`) join `ConcreteSubject`);

-- 2021-06-14 14:18:30
