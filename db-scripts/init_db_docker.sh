#!/bin/sh

PROJECT_DIR=data-management-proj
DB_COMPOSER_SEVICE_NAME=dm_db_1
echo "${PROJECT_DIR}_${DB_COMPOSER_SEVICE_NAME}"

docker exec -it "${PROJECT_DIR}_${DB_COMPOSER_SEVICE_NAME}" sh -c "sh /db-scripts/init_database.sh"
