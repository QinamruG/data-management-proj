-- Adminer 4.7.8 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;

USE `load`;

DELIMITER ;;

DROP FUNCTION IF EXISTS `is_all_StudyLoads_has_teacher_replacement`;;
CREATE FUNCTION `is_all_StudyLoads_has_teacher_replacement`(`TeacherCode` int(255)) RETURNS tinyint(1)
begin
    DECLARE done INT DEFAULT FALSE;
    DECLARE IsAllGood TINYINT DEFAULT 1;
    DECLARE Snum INT;
    DECLARE cur CURSOR FOR SELECT subjectNumber from StudyLoad
    where teacherCode = TeacherCode group by subjectNumber;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    Open cur;
    read_loop: LOOP

        FETCH cur INTO Snum;
        IF done THEN
            LEAVE read_loop;
        END IF;
if(Snum > 0) then

        IF NOT EXISTS
            (select * from Teacher t join SubjectList list on t.teacherCode = list.teacherCode
            where list.subjectNumber in(
            select sl.subjectNumber from Teacher t
            JOIN StudyLoad sl on t.teacherCode = sl.TeacherCode
            where sl.teacherCode = TeacherCode)
            and t.teacherCode != TeacherCode
            and list.subjectNumber = Snum)
        THEN set IsAllGood = 0;
             LEAVE read_loop;
        END IF;
end if;
    END LOOP;
    Close cur;
    return IsAllGood;
end;;

DROP FUNCTION IF EXISTS `is_other_teachers_have_subject`;;
CREATE FUNCTION `is_other_teachers_have_subject`(`TeacherCode` int(255), `SubjectNumber` int(255)) RETURNS tinyint(2)
begin
    declare isSubjectExists tinyint;
    if exists(select * from SubjectList sl
    where sl.subjectNumber = SubjectNumber and sl.teacherCode != TeacherCode)
    then set isSubjectExists = 1;
    else set isSubjectExists = 0;
    end if;
    return isSubjectExists;
end;;

DROP FUNCTION IF EXISTS `testf`;;
CREATE FUNCTION `testf`(`tcod` int) RETURNS int(11)
begin
    DECLARE done INT DEFAULT FALSE;
    DECLARE IsAllGood TINYINT DEFAULT 0;
    DECLARE Snum INT;
    DECLARE cur CURSOR FOR SELECT subjectNumber from StudyLoad
    where teacherCode = tcod group by subjectNumber;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    Open cur;
    read_loop: LOOP

        FETCH cur INTO Snum;
        IF done THEN
            LEAVE read_loop;
        END IF;

set IsAllGood = IsAllGood + Snum;

    END LOOP;
    Close cur;
    return IsAllGood;
end;;

DROP PROCEDURE IF EXISTS `changeTeacherInStudyLoads`;;
CREATE PROCEDURE `changeTeacherInStudyLoads`(IN `TeacherCode` int(255))
begin
    update StudyLoad stl
    set teacherCode = (select t.teacherCode from Teacher t
    join SubjectList sl on t.teacherCode = sl.teacherCode
    where t.teacherCode != TeacherCode
    and sl.subjectNumber = stl.subjectNumber limit 1)
    where stl.teacherCode = TeacherCode;
end;;

DROP PROCEDURE IF EXISTS `changeTeacherInStudyLoadsWithThatSubject`;;
CREATE PROCEDURE `changeTeacherInStudyLoadsWithThatSubject`(IN `SubjectNumber` int(255), IN `TeacherCode` int(255))
begin
    update StudyLoad stl
    set stl.teacherCode = (select t.teacherCode from Teacher t
    join SubjectList sl on t.teacherCode = sl.teacherCode
    where t.teacherCode != TeacherCode
    and sl.subjectNumber = stl.subjectNumber limit 1)
    where stl.teacherCode = TeacherCode
    and stl.subjectNumber = SubjectNumber;
end;;

DROP PROCEDURE IF EXISTS `return_teachers_with_same_subjects`;;
CREATE PROCEDURE `return_teachers_with_same_subjects`(IN `TeacherCode` int, IN `SubjectNumber` int)
begin
    select * from Teacher t join SubjectList list on t.teacherCode = list.teacherCode
    where list.subjectNumber in(
    select sl.subjectNumber from Teacher t
    join StudyLoad sl on t.teacherCode = sl.TeacherCode
    where sl.teacherCode = TeacherCode)
    and t.teacherCode != TeacherCode
    and list.subjectNumber = SubjectNumber;
end;;

DELIMITER ;

-- 2021-06-14 10:54:16
